-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 02/06/2020 às 11:32
-- Versão do servidor: 5.7.30-0ubuntu0.18.04.1
-- Versão do PHP: 7.2.24-0ubuntu0.18.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `projetos_prohawk_site`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `ph_commentmeta`
--

CREATE TABLE `ph_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `ph_comments`
--

CREATE TABLE `ph_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Fazendo dump de dados para tabela `ph_comments`
--

INSERT INTO `ph_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Um comentarista do WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2020-04-29 14:34:38', '2020-04-29 17:34:38', 'Olá, isso é um comentário.\nPara começar a moderar, editar e excluir comentários, visite a tela de Comentários no painel.\nAvatares de comentaristas vêm a partir do <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `ph_links`
--

CREATE TABLE `ph_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `ph_options`
--

CREATE TABLE `ph_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Fazendo dump de dados para tabela `ph_options`
--

INSERT INTO `ph_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/projetos/prohawk_site', 'yes'),
(2, 'home', 'http://localhost/projetos/prohawk_site', 'yes'),
(3, 'blogname', 'Prohawk', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'dev@gran.ag', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '', 'yes'),
(29, 'rewrite_rules', '', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '47018', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'America/Sao_Paulo', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '16', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'admin_email_lifespan', '1603733654', 'yes'),
(94, 'initial_db_version', '47018', 'yes'),
(95, 'ph_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(96, 'fresh_site', '0', 'yes'),
(97, 'WPLANG', 'pt_BR', 'yes'),
(98, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(104, 'cron', 'a:9:{i:1591108485;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1591119283;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1591119285;a:1:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1591119286;a:2:{s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1591119306;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1591119309;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1591121783;a:1:{s:21:\"ai1wm_storage_cleanup\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1591292083;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}', 'yes'),
(105, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(115, 'recovery_keys', 'a:0:{}', 'yes'),
(116, 'theme_mods_twentytwenty', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1588182948;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}', 'yes'),
(134, 'can_compress_scripts', '0', 'no'),
(137, 'recently_activated', 'a:0:{}', 'yes'),
(149, 'current_theme', 'Prohawk', 'yes'),
(150, 'theme_mods_twentynineteen', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1588183001;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(151, 'theme_switched', '', 'yes'),
(152, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.1.8\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1588172178;s:7:\"version\";s:5:\"5.1.7\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(156, 'theme_mods_prohawk', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:6:\"menu-1\";i:2;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(157, 'recovery_mode_email_last_sent', '1588184170', 'yes'),
(158, 'redux_version_upgraded_from', '3.6.18', 'yes'),
(159, 'r_notice_data', '{}', 'yes'),
(160, 'redux_blast', '1588184713', 'yes'),
(161, 'configuracao', 'a:33:{s:8:\"last_tab\";s:1:\"1\";s:8:\"opt_logo\";a:9:{s:3:\"url\";s:74:\"http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/logo.svg\";s:2:\"id\";s:2:\"63\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:75:\"http://localhost/projetos/prohawk_site/wp-includes/images/media/default.png\";s:5:\"title\";s:4:\"logo\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:15:\"opt_logo_footer\";a:9:{s:3:\"url\";s:80:\"http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/logofooter.png\";s:2:\"id\";s:2:\"92\";s:6:\"height\";s:3:\"129\";s:5:\"width\";s:3:\"648\";s:9:\"thumbnail\";s:88:\"http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/logofooter-150x129.png\";s:5:\"title\";s:10:\"logofooter\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:9:\"opt_selos\";s:11:\"70,67,69,68\";s:9:\"opt_email\";s:22:\"contato@prohawk.com.br\";s:18:\"opt_whatsapp_texto\";s:16:\"+55 41 3552-5973\";s:12:\"opt_whatsapp\";s:12:\"554135525973\";s:12:\"opt_facebook\";s:38:\"https://pt-br.facebook.com/prohawkeng/\";s:13:\"opt_instagram\";s:44:\"https://www.instagram.com/prohawk.eng/?hl=en\";s:11:\"opt_youtube\";s:43:\"https://www.youtube.com/watch?v=pWiGvV-ooxQ\";s:12:\"opt_linkedin\";s:39:\"https://br.linkedin.com/company/prohawk\";s:25:\"opt_titulo_secao_destaque\";s:0:\"\";s:32:\"opt_sobre_prohawk_secao_destaque\";a:5:{i:0;s:13:\"é agilidade.\";i:1;s:13:\"é precisão.\";i:2;s:14:\"é tecnologia.\";i:3;s:13:\"é qualidade.\";i:4;s:22:\"é sua melhor escolha.\";}s:28:\"opt_descricao_secao_destaque\";s:155:\"A Prohawk é uma empresa inovadora que traz ao mercado uma forma <br>diferenciada e eficaz na execução de aerolevantamento e projetos para infraestrutura\";s:25:\"opt_banner_secao_destaque\";a:9:{s:3:\"url\";s:76:\"http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/banner.png\";s:2:\"id\";s:2:\"60\";s:6:\"height\";s:4:\"2160\";s:5:\"width\";s:4:\"5100\";s:9:\"thumbnail\";s:84:\"http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/banner-150x150.png\";s:5:\"title\";s:6:\"banner\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:16:\"opt_titulo_sobre\";s:97:\"Especialista em mapeamento e projetos para infraestrutura Energia, saneamento, ferrovia e rodovia\";s:15:\"opt_texto_sobre\";s:538:\"A PROHAWK oferece serviços tecnológicos através de levantamento aéreos com o uso de RPA (Remotely Piloted Aircraft) oferecendo soluções completas e inteligentes para topografia, mapeamento de pequenas e grandes áreas, georreferenciamento e agrimensura com precisão, alto rendimento, rapidez e baixo custo. Nossa equipe conta com engenheiros capacitados para gerenciar as operações e processamento de todos os dados captados, gerando produtos úteis e atestados aos padrões técnicos que possam substituir modelos convencionais.\";s:16:\"opt_imagem_sobre\";a:9:{s:3:\"url\";s:77:\"http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/sobre-1.png\";s:2:\"id\";s:2:\"90\";s:6:\"height\";s:3:\"961\";s:5:\"width\";s:3:\"626\";s:9:\"thumbnail\";s:85:\"http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/sobre-1-150x150.png\";s:5:\"title\";s:5:\"sobre\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:19:\"opt_icone_seguranca\";a:9:{s:3:\"url\";s:74:\"http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/selo.png\";s:2:\"id\";s:2:\"66\";s:6:\"height\";s:3:\"147\";s:5:\"width\";s:3:\"147\";s:9:\"thumbnail\";s:74:\"http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/selo.png\";s:5:\"title\";s:4:\"selo\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:20:\"opt_titulo_seguranca\";s:45:\"Faça seu projeto com segurança e Inovação\";s:23:\"opt_descricao_seguranca\";s:269:\"A PROHAWK foi um das primeiras e é uma das únicas empresas de mapeamento aéreo com uso de RPA (Remotely Piloted Aircraft / Drone) a obter a regulamentação pelo Ministério da Defesa na categoria A no Brasil, que abrange o aerolevantamento e processamento de dados.\";s:31:\"opt_titulo_importante_seguranca\";s:11:\"Importante!\";s:27:\"opt_p1_importante_seguranca\";s:175:\"Sem autorização e cadastramento da empresa prestadora de aerolevantamento dentro destes 4 órgãos, qualquer serviço ou projeto feito oriundo de aerolevantamento é ILEGAL!\";s:27:\"opt_p2_importante_seguranca\";s:361:\"Tendo como consequência citada pela “PORTARIA NORMATIVA N° 101/GM-MD, DE 26 DE DEZEMBRO DE 2018 Art. 63. As entidades não inscritas que realizarem irregularmente a atividade de aerolevantamento estão sujeitas a responder civil e penalmente pelo ato irregular, assim como os respectivos contratantes.” E não terão seus levantamentos e projetos válidos\";s:24:\"opt_imagem_areas_atuacao\";a:9:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";s:5:\"title\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:23:\"opt_drone_areas_atuacao\";a:9:{s:3:\"url\";s:83:\"http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/droneservicos.png\";s:2:\"id\";s:2:\"62\";s:6:\"height\";s:3:\"455\";s:5:\"width\";s:3:\"828\";s:9:\"thumbnail\";s:91:\"http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/droneservicos-150x150.png\";s:5:\"title\";s:13:\"droneservicos\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:24:\"opt_drone2_areas_atuacao\";a:9:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";s:5:\"title\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:24:\"opt_titulo_areas_atuacao\";s:30:\"Especialista em infraestrutura\";s:20:\"opt_titulo_vantagens\";s:0:\"\";s:19:\"opt_titulo_servicos\";s:20:\"Produtos <br>gerados\";s:20:\"opt_titulo_copyright\";s:44:\"Ⓒ 2020 PROHAWK. Todos direitos reservados.\";s:22:\"opt_endereco_copyright\";s:61:\"Alameda Prudente de Moraes, 536 - Curitiba/PR - CEP 80430-220\";s:19:\"opt-customizer-only\";s:1:\"2\";}', 'yes'),
(162, 'configuracao-transients', 'a:2:{s:14:\"changed_values\";a:1:{s:20:\"opt_titulo_vantagens\";s:29:\"Prohawk é sua melhor escolha\";}s:9:\"last_save\";i:1590516011;}', 'yes'),
(198, 'new_admin_email', 'dev@gran.ag', 'yes'),
(224, 'jetpack_active_modules', 'a:0:{}', 'yes'),
(230, 'ai1wm_updater', 'a:0:{}', 'yes'),
(293, 'category_children', 'a:0:{}', 'yes'),
(381, 'cpto_options', 'a:7:{s:23:\"show_reorder_interfaces\";a:6:{s:4:\"post\";s:4:\"show\";s:10:\"attachment\";s:4:\"show\";s:8:\"wp_block\";s:4:\"show\";s:13:\"areas_atuacao\";s:4:\"show\";s:8:\"vantagem\";s:4:\"show\";s:7:\"servico\";s:4:\"show\";}s:8:\"autosort\";i:1;s:9:\"adminsort\";i:1;s:18:\"use_query_ASC_DESC\";s:0:\"\";s:17:\"archive_drag_drop\";i:1;s:10:\"capability\";s:14:\"manage_options\";s:21:\"navigation_sort_apply\";i:1;}', 'yes'),
(382, 'CPT_configured', 'TRUE', 'yes'),
(419, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(567, 'active_plugins', 'a:5:{i:0;s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";i:1;s:35:\"redux-framework/redux-framework.php\";i:2;s:36:\"contact-form-7/wp-contact-form-7.php\";i:3;s:21:\"meta-box/meta-box.php\";i:4;s:37:\"post-types-order/post-types-order.php\";}', 'yes'),
(568, 'ai1wm_secret_key', 'o1lMzZj24XXM', 'yes'),
(569, 'ai1wm_backups_labels', 'a:0:{}', 'yes'),
(570, 'ai1wm_sites_links', 'a:0:{}', 'yes'),
(573, 'ai1wm_status', 'a:2:{s:4:\"type\";s:8:\"download\";s:7:\"message\";s:348:\"<a href=\"http://localhost/projetos/prohawk_site/wp-content/ai1wm-backups/localhost-projetos-prohawk_site-20200527-004353-d8m10y.wpress\" class=\"ai1wm-button-green ai1wm-emphasize ai1wm-button-download\" title=\"localhost\" download=\"localhost-projetos-prohawk_site-20200527-004353-d8m10y.wpress\"><span>Download localhost</span><em>Size: 424 MB</em></a>\";}', 'yes'),
(583, 'template', 'prohawk', 'yes'),
(584, 'stylesheet', 'prohawk', 'yes'),
(585, '_site_transient_timeout_browser_81dd33670f154df1ac6887eccc23e632', '1591119931', 'no'),
(586, '_site_transient_browser_81dd33670f154df1ac6887eccc23e632', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"80.0.3987.149\";s:8:\"platform\";s:5:\"Linux\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(587, '_site_transient_timeout_php_check_2202b90c2476ab61f02773f6f7bdb2a9', '1591119932', 'no'),
(588, '_site_transient_php_check_2202b90c2476ab61f02773f6f7bdb2a9', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(607, '_site_transient_timeout_theme_roots', '1591109326', 'no'),
(608, '_site_transient_theme_roots', 'a:2:{s:7:\"prohawk\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";}', 'no'),
(610, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:3:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.4.1.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.4.1.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.4.1\";s:7:\"version\";s:5:\"5.4.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.4.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.4.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.4.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.4.1-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.4.1-partial-0.zip\";s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.4.1\";s:7:\"version\";s:5:\"5.4.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:3:\"5.4\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.4.1.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.4.1.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.4.1\";s:7:\"version\";s:5:\"5.4.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}}s:12:\"last_checked\";i:1591107531;s:15:\"version_checked\";s:3:\"5.4\";s:12:\"translations\";a:0:{}}', 'no'),
(611, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1591107532;s:7:\"checked\";a:2:{s:7:\"prohawk\";s:3:\"1.0\";s:14:\"twentynineteen\";s:3:\"1.5\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(612, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1591107534;s:7:\"checked\";a:8:{s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";s:4:\"7.22\";s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:5:\"4.4.3\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.1.8\";s:21:\"meta-box/meta-box.php\";s:5:\"5.3.0\";s:37:\"post-types-order/post-types-order.php\";s:7:\"1.9.4.3\";s:47:\"really-simple-ssl/rlrsssl-really-simple-ssl.php\";s:5:\"3.3.3\";s:35:\"redux-framework/redux-framework.php\";s:6:\"3.6.18\";s:24:\"wordpress-seo/wp-seo.php\";s:4:\"14.1\";}s:8:\"response\";a:2:{s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.1.9\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.1.9.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=2279696\";s:2:\"1x\";s:67:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=2279696\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.4.1\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:4:\"14.2\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wordpress-seo.14.2.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=1834347\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1946641\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1946641\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435\";}s:6:\"tested\";s:5:\"5.4.1\";s:12:\"requires_php\";s:6:\"5.6.20\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:5:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:23:\"all-in-one-wp-migration\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:4:\"7.22\";s:7:\"updated\";s:19:\"2020-05-18 14:10:20\";s:7:\"package\";s:89:\"https://downloads.wordpress.org/translation/plugin/all-in-one-wp-migration/7.22/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"contact-form-7\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"5.1.8\";s:7:\"updated\";s:19:\"2020-04-10 16:37:58\";s:7:\"package\";s:81:\"https://downloads.wordpress.org/translation/plugin/contact-form-7/5.1.8/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:2;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:8:\"meta-box\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"5.3.0\";s:7:\"updated\";s:19:\"2019-07-13 02:22:41\";s:7:\"package\";s:75:\"https://downloads.wordpress.org/translation/plugin/meta-box/5.3.0/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:3;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:17:\"really-simple-ssl\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"3.3.3\";s:7:\"updated\";s:19:\"2020-04-10 06:35:44\";s:7:\"package\";s:84:\"https://downloads.wordpress.org/translation/plugin/really-simple-ssl/3.3.3/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:4;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:13:\"wordpress-seo\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:4:\"14.1\";s:7:\"updated\";s:19:\"2020-05-07 20:52:26\";s:7:\"package\";s:79:\"https://downloads.wordpress.org/translation/plugin/wordpress-seo/14.1/pt_BR.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:6:{s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:37:\"w.org/plugins/all-in-one-wp-migration\";s:4:\"slug\";s:23:\"all-in-one-wp-migration\";s:6:\"plugin\";s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";s:11:\"new_version\";s:4:\"7.22\";s:3:\"url\";s:54:\"https://wordpress.org/plugins/all-in-one-wp-migration/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/all-in-one-wp-migration.7.22.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:76:\"https://ps.w.org/all-in-one-wp-migration/assets/icon-256x256.png?rev=2246309\";s:2:\"1x\";s:76:\"https://ps.w.org/all-in-one-wp-migration/assets/icon-128x128.png?rev=2246309\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:79:\"https://ps.w.org/all-in-one-wp-migration/assets/banner-1544x500.png?rev=2246309\";s:2:\"1x\";s:78:\"https://ps.w.org/all-in-one-wp-migration/assets/banner-772x250.png?rev=2246309\";}s:11:\"banners_rtl\";a:0:{}}s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:49:\"w.org/plugins/all-in-one-wp-security-and-firewall\";s:4:\"slug\";s:35:\"all-in-one-wp-security-and-firewall\";s:6:\"plugin\";s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:11:\"new_version\";s:5:\"4.4.3\";s:3:\"url\";s:66:\"https://wordpress.org/plugins/all-in-one-wp-security-and-firewall/\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/plugin/all-in-one-wp-security-and-firewall.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:88:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/icon-128x128.png?rev=1232826\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:91:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/banner-1544x500.png?rev=1914011\";s:2:\"1x\";s:90:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/banner-772x250.png?rev=1914013\";}s:11:\"banners_rtl\";a:0:{}}s:21:\"meta-box/meta-box.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:22:\"w.org/plugins/meta-box\";s:4:\"slug\";s:8:\"meta-box\";s:6:\"plugin\";s:21:\"meta-box/meta-box.php\";s:11:\"new_version\";s:5:\"5.3.0\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/meta-box/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/meta-box.5.3.0.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/meta-box/assets/icon-128x128.png?rev=1100915\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:63:\"https://ps.w.org/meta-box/assets/banner-772x250.png?rev=1929588\";}s:11:\"banners_rtl\";a:0:{}}s:37:\"post-types-order/post-types-order.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:30:\"w.org/plugins/post-types-order\";s:4:\"slug\";s:16:\"post-types-order\";s:6:\"plugin\";s:37:\"post-types-order/post-types-order.php\";s:11:\"new_version\";s:7:\"1.9.4.3\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/post-types-order/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/post-types-order.1.9.4.3.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/post-types-order/assets/icon-128x128.png?rev=1226428\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/post-types-order/assets/banner-1544x500.png?rev=1675574\";s:2:\"1x\";s:71:\"https://ps.w.org/post-types-order/assets/banner-772x250.png?rev=1429949\";}s:11:\"banners_rtl\";a:0:{}}s:47:\"really-simple-ssl/rlrsssl-really-simple-ssl.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:31:\"w.org/plugins/really-simple-ssl\";s:4:\"slug\";s:17:\"really-simple-ssl\";s:6:\"plugin\";s:47:\"really-simple-ssl/rlrsssl-really-simple-ssl.php\";s:11:\"new_version\";s:5:\"3.3.3\";s:3:\"url\";s:48:\"https://wordpress.org/plugins/really-simple-ssl/\";s:7:\"package\";s:66:\"https://downloads.wordpress.org/plugin/really-simple-ssl.3.3.3.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:70:\"https://ps.w.org/really-simple-ssl/assets/icon-128x128.png?rev=1782452\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/really-simple-ssl/assets/banner-1544x500.png?rev=2263742\";s:2:\"1x\";s:72:\"https://ps.w.org/really-simple-ssl/assets/banner-772x250.jpg?rev=1881345\";}s:11:\"banners_rtl\";a:0:{}}s:35:\"redux-framework/redux-framework.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/redux-framework\";s:4:\"slug\";s:15:\"redux-framework\";s:6:\"plugin\";s:35:\"redux-framework/redux-framework.php\";s:11:\"new_version\";s:6:\"3.6.18\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/redux-framework/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/redux-framework.3.6.18.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/redux-framework/assets/icon-256x256.png?rev=995554\";s:2:\"1x\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";s:3:\"svg\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/redux-framework/assets/banner-772x250.png?rev=793165\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(613, '_transient_health-check-site-status-result', '{\"good\":11,\"recommended\":5,\"critical\":1}', 'yes');

-- --------------------------------------------------------

--
-- Estrutura para tabela `ph_postmeta`
--

CREATE TABLE `ph_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Fazendo dump de dados para tabela `ph_postmeta`
--

INSERT INTO `ph_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(9, 16, '_edit_lock', '1588261226:1'),
(10, 16, '_wp_page_template', 'pages/inicial.php'),
(17, 20, '_edit_last', '1'),
(18, 20, '_edit_lock', '1589990164:1'),
(33, 27, '_edit_last', '1'),
(34, 27, '_edit_lock', '1589990164:1'),
(37, 28, '_edit_last', '1'),
(38, 28, '_edit_lock', '1589990165:1'),
(41, 29, '_edit_last', '1'),
(42, 29, '_edit_lock', '1589990165:1'),
(45, 30, '_edit_last', '1'),
(46, 30, '_edit_lock', '1589990165:1'),
(49, 31, '_edit_last', '1'),
(50, 31, '_edit_lock', '1589990165:1'),
(53, 32, '_edit_last', '1'),
(54, 32, '_edit_lock', '1589990165:1'),
(57, 33, '_edit_last', '1'),
(58, 33, '_edit_lock', '1589990166:1'),
(61, 34, '_edit_last', '1'),
(62, 34, '_edit_lock', '1589911005:1'),
(64, 35, '_edit_last', '1'),
(65, 35, '_edit_lock', '1589911008:1'),
(67, 36, '_edit_last', '1'),
(68, 36, '_edit_lock', '1589911006:1'),
(70, 37, '_edit_last', '1'),
(71, 37, '_edit_lock', '1589911008:1'),
(73, 38, '_edit_last', '1'),
(74, 38, '_edit_lock', '1589911007:1'),
(76, 39, '_edit_last', '1'),
(77, 39, '_edit_lock', '1589911008:1'),
(79, 40, '_edit_last', '1'),
(80, 40, '_edit_lock', '1589911007:1'),
(82, 41, '_edit_last', '1'),
(83, 41, '_edit_lock', '1589911009:1'),
(85, 42, '_edit_last', '1'),
(86, 42, '_edit_lock', '1589911007:1'),
(88, 43, '_edit_last', '1'),
(89, 43, '_edit_lock', '1589911009:1'),
(91, 44, '_edit_last', '1'),
(92, 44, '_edit_lock', '1589911613:1'),
(94, 45, '_edit_last', '1'),
(95, 45, '_edit_lock', '1589911614:1'),
(97, 46, '_edit_last', '1'),
(98, 46, '_edit_lock', '1589911614:1'),
(100, 47, '_edit_last', '1'),
(101, 47, '_edit_lock', '1589911614:1'),
(103, 48, '_edit_last', '1'),
(104, 48, '_edit_lock', '1589911615:1'),
(106, 49, '_edit_last', '1'),
(107, 49, '_edit_lock', '1589911615:1'),
(109, 50, '_edit_last', '1'),
(110, 50, '_edit_lock', '1589911615:1'),
(112, 51, '_edit_last', '1'),
(113, 51, '_edit_lock', '1589911615:1'),
(115, 52, '_edit_last', '1'),
(116, 52, '_edit_lock', '1589911616:1'),
(118, 53, '_edit_last', '1'),
(119, 53, '_edit_lock', '1589911616:1'),
(121, 54, '_edit_last', '1'),
(122, 54, '_edit_lock', '1589911617:1'),
(126, 56, '_form', '<label for=\"input-nome\">[text* input-nome id:input-nome placeholder \"Nome\"]</label><label for=\"input-email\">[email* input-email id:input-email placeholder \"Email\"]</label><label for=\"input-celular\">[tel* input-celular id:input-celular placeholder \"Celular\"]</label><div class=\"div-button-enviar\">[submit \"Enviar\"]</div>'),
(127, 56, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:26:\"Prohawk \"Formulário site\"\";s:6:\"sender\";s:21:\"Prohawk <dev@gran.ag>\";s:9:\"recipient\";s:11:\"dev@gran.ag\";s:4:\"body\";s:160:\"De: [input-nome]\nEmail: [input-email]\nCelular: [input-celular]\n\n-- \nThis e-mail was sent from a contact form on Prohawk (http://localhost/projetos/prohawk_site)\";s:18:\"additional_headers\";s:23:\"Reply-To: [input-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(128, 56, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:24:\"Prohawk \"[your-subject]\"\";s:6:\"sender\";s:21:\"Prohawk <dev@gran.ag>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:126:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Prohawk (http://localhost/projetos/prohawk_site)\";s:18:\"additional_headers\";s:21:\"Reply-To: dev@gran.ag\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(129, 56, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(130, 56, '_additional_settings', ''),
(131, 56, '_locale', 'pt_BR'),
(158, 56, '_config_errors', 'a:1:{s:11:\"mail.sender\";a:1:{i:0;a:2:{s:4:\"code\";i:103;s:4:\"args\";a:3:{s:7:\"message\";s:0:\"\";s:6:\"params\";a:0:{}s:4:\"link\";s:70:\"https://contactform7.com/configuration-errors/email-not-in-site-domain\";}}}}'),
(159, 60, '_wp_attached_file', '2020/05/banner.png'),
(160, 60, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:5100;s:6:\"height\";i:2160;s:4:\"file\";s:18:\"2020/05/banner.png\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"banner-300x127.png\";s:5:\"width\";i:300;s:6:\"height\";i:127;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"banner-1024x434.png\";s:5:\"width\";i:1024;s:6:\"height\";i:434;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"banner-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"banner-768x325.png\";s:5:\"width\";i:768;s:6:\"height\";i:325;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:19:\"banner-1536x651.png\";s:5:\"width\";i:1536;s:6:\"height\";i:651;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:19:\"banner-2048x867.png\";s:5:\"width\";i:2048;s:6:\"height\";i:867;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(161, 61, '_wp_attached_file', '2020/05/drone.png'),
(162, 61, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1278;s:6:\"height\";i:766;s:4:\"file\";s:17:\"2020/05/drone.png\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"drone-300x180.png\";s:5:\"width\";i:300;s:6:\"height\";i:180;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:18:\"drone-1024x614.png\";s:5:\"width\";i:1024;s:6:\"height\";i:614;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"drone-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"drone-768x460.png\";s:5:\"width\";i:768;s:6:\"height\";i:460;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(163, 62, '_wp_attached_file', '2020/05/droneservicos.png'),
(164, 62, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:828;s:6:\"height\";i:455;s:4:\"file\";s:25:\"2020/05/droneservicos.png\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"droneservicos-300x165.png\";s:5:\"width\";i:300;s:6:\"height\";i:165;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"droneservicos-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:25:\"droneservicos-768x422.png\";s:5:\"width\";i:768;s:6:\"height\";i:422;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(165, 63, '_wp_attached_file', '2020/05/logo.svg'),
(166, 64, '_wp_attached_file', '2020/05/logofooter.svg'),
(167, 65, '_wp_attached_file', '2020/05/produtos.png'),
(168, 65, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:549;s:6:\"height\";i:320;s:4:\"file\";s:20:\"2020/05/produtos.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"produtos-300x175.png\";s:5:\"width\";i:300;s:6:\"height\";i:175;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"produtos-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(169, 66, '_wp_attached_file', '2020/05/selo.png'),
(170, 66, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:16:\"2020/05/selo.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(171, 67, '_wp_attached_file', '2020/05/selo1.png'),
(172, 67, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:59;s:6:\"height\";i:81;s:4:\"file\";s:17:\"2020/05/selo1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(173, 68, '_wp_attached_file', '2020/05/selo2.png'),
(174, 68, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:79;s:6:\"height\";i:81;s:4:\"file\";s:17:\"2020/05/selo2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(175, 69, '_wp_attached_file', '2020/05/selo3.png'),
(176, 69, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:68;s:6:\"height\";i:81;s:4:\"file\";s:17:\"2020/05/selo3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(177, 70, '_wp_attached_file', '2020/05/selo4.png'),
(178, 70, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:76;s:6:\"height\";i:81;s:4:\"file\";s:17:\"2020/05/selo4.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(179, 71, '_wp_attached_file', '2020/05/servico1.svg'),
(180, 72, '_wp_attached_file', '2020/05/servico2.svg'),
(181, 73, '_wp_attached_file', '2020/05/servico3.svg'),
(182, 74, '_wp_attached_file', '2020/05/servico4.svg'),
(183, 75, '_wp_attached_file', '2020/05/servico5.svg'),
(184, 76, '_wp_attached_file', '2020/05/servico6.svg'),
(185, 77, '_wp_attached_file', '2020/05/servico7.svg'),
(186, 78, '_wp_attached_file', '2020/05/servico8.svg'),
(189, 80, '_wp_attached_file', '2020/05/vantagem1.svg'),
(190, 81, '_wp_attached_file', '2020/05/vantagem2.svg'),
(191, 82, '_wp_attached_file', '2020/05/vantagem3.svg'),
(192, 83, '_wp_attached_file', '2020/05/vantagem4.svg'),
(193, 84, '_wp_attached_file', '2020/05/vantagem5.svg'),
(194, 85, '_wp_attached_file', '2020/05/vantagem6.svg'),
(195, 86, '_wp_attached_file', '2020/05/vantagem7.svg'),
(196, 87, '_wp_attached_file', '2020/05/vantagem8.svg'),
(197, 88, '_wp_attached_file', '2020/05/vantagem9.svg'),
(198, 89, '_wp_attached_file', '2020/05/vantagem10.svg'),
(199, 90, '_wp_attached_file', '2020/05/sobre-1.png'),
(200, 90, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:626;s:6:\"height\";i:961;s:4:\"file\";s:19:\"2020/05/sobre-1.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"sobre-1-195x300.png\";s:5:\"width\";i:195;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"sobre-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(201, 33, '_thumbnail_id', '71'),
(203, 32, '_thumbnail_id', '72'),
(205, 31, '_thumbnail_id', '73'),
(207, 30, '_thumbnail_id', '74'),
(209, 29, '_thumbnail_id', '75'),
(211, 28, '_thumbnail_id', '76'),
(213, 27, '_thumbnail_id', '77'),
(215, 20, '_thumbnail_id', '78'),
(217, 43, '_thumbnail_id', '80'),
(218, 41, '_thumbnail_id', '81'),
(219, 39, '_thumbnail_id', '82'),
(220, 37, '_thumbnail_id', '83'),
(221, 35, '_thumbnail_id', '84'),
(222, 42, '_thumbnail_id', '85'),
(223, 40, '_thumbnail_id', '86'),
(224, 38, '_thumbnail_id', '87'),
(225, 36, '_thumbnail_id', '88'),
(226, 34, '_thumbnail_id', '89'),
(227, 91, '_wp_attached_file', '2020/05/produtos-1.png'),
(228, 91, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:549;s:6:\"height\";i:320;s:4:\"file\";s:22:\"2020/05/produtos-1.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"produtos-1-300x175.png\";s:5:\"width\";i:300;s:6:\"height\";i:175;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"produtos-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(229, 54, 'prohawk_galeria_servico', '65'),
(230, 54, 'prohawk_galeria_servico', '91'),
(231, 53, 'prohawk_galeria_servico', '91'),
(232, 53, 'prohawk_galeria_servico', '65'),
(233, 52, 'prohawk_galeria_servico', '91'),
(234, 52, 'prohawk_galeria_servico', '65'),
(235, 51, 'prohawk_galeria_servico', '91'),
(236, 51, 'prohawk_galeria_servico', '65'),
(237, 50, 'prohawk_galeria_servico', '91'),
(238, 50, 'prohawk_galeria_servico', '65'),
(239, 49, 'prohawk_galeria_servico', '91'),
(240, 49, 'prohawk_galeria_servico', '65'),
(241, 48, 'prohawk_galeria_servico', '91'),
(242, 48, 'prohawk_galeria_servico', '65'),
(243, 47, 'prohawk_galeria_servico', '91'),
(244, 47, 'prohawk_galeria_servico', '65'),
(245, 46, 'prohawk_galeria_servico', '91'),
(246, 46, 'prohawk_galeria_servico', '65'),
(247, 45, 'prohawk_galeria_servico', '91'),
(248, 45, 'prohawk_galeria_servico', '65'),
(249, 44, 'prohawk_galeria_servico', '91'),
(250, 44, 'prohawk_galeria_servico', '65'),
(251, 92, '_wp_attached_file', '2020/05/logofooter.png'),
(252, 92, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:648;s:6:\"height\";i:129;s:4:\"file\";s:22:\"2020/05/logofooter.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"logofooter-300x60.png\";s:5:\"width\";i:300;s:6:\"height\";i:60;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"logofooter-150x129.png\";s:5:\"width\";i:150;s:6:\"height\";i:129;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(253, 93, '_wp_attached_file', '2020/04/servico1b.svg'),
(254, 94, '_wp_attached_file', '2020/04/servico2b.svg'),
(255, 95, '_wp_attached_file', '2020/04/servico3b.svg'),
(256, 96, '_wp_attached_file', '2020/04/servico4b.svg'),
(257, 97, '_wp_attached_file', '2020/04/servico5b.svg'),
(258, 98, '_wp_attached_file', '2020/04/servico6b.svg'),
(259, 99, '_wp_attached_file', '2020/04/servico7b.svg'),
(260, 100, '_wp_attached_file', '2020/04/servico8b.svg'),
(261, 33, 'prohawk_icone_hover', '93'),
(262, 32, 'prohawk_icone_hover', '94'),
(263, 31, 'prohawk_icone_hover', '95'),
(264, 30, 'prohawk_icone_hover', '96'),
(265, 29, 'prohawk_icone_hover', '97'),
(266, 28, 'prohawk_icone_hover', '98'),
(267, 27, 'prohawk_icone_hover', '99'),
(268, 20, 'prohawk_icone_hover', '100'),
(269, 101, '_menu_item_type', 'post_type'),
(270, 101, '_menu_item_menu_item_parent', '0'),
(271, 101, '_menu_item_object_id', '16'),
(272, 101, '_menu_item_object', 'page'),
(273, 101, '_menu_item_target', ''),
(274, 101, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(275, 101, '_menu_item_xfn', ''),
(276, 101, '_menu_item_url', ''),
(277, 101, '_menu_item_orphaned', '1590002538'),
(278, 102, '_menu_item_type', 'custom'),
(279, 102, '_menu_item_menu_item_parent', '0'),
(280, 102, '_menu_item_object_id', '102'),
(281, 102, '_menu_item_object', 'custom'),
(282, 102, '_menu_item_target', ''),
(283, 102, '_menu_item_classes', 'a:1:{i:0;s:9:\"scrollTop\";}'),
(284, 102, '_menu_item_xfn', ''),
(285, 102, '_menu_item_url', '#secao-sobre'),
(287, 103, '_menu_item_type', 'custom'),
(288, 103, '_menu_item_menu_item_parent', '0'),
(289, 103, '_menu_item_object_id', '103'),
(290, 103, '_menu_item_object', 'custom'),
(291, 103, '_menu_item_target', ''),
(292, 103, '_menu_item_classes', 'a:1:{i:0;s:9:\"scrollTop\";}'),
(293, 103, '_menu_item_xfn', ''),
(294, 103, '_menu_item_url', '#secao-areas-atuacao'),
(296, 104, '_menu_item_type', 'custom'),
(297, 104, '_menu_item_menu_item_parent', '0'),
(298, 104, '_menu_item_object_id', '104'),
(299, 104, '_menu_item_object', 'custom'),
(300, 104, '_menu_item_target', ''),
(301, 104, '_menu_item_classes', 'a:1:{i:0;s:9:\"scrollTop\";}'),
(302, 104, '_menu_item_xfn', ''),
(303, 104, '_menu_item_url', '#secao-vantagens'),
(305, 105, '_menu_item_type', 'custom'),
(306, 105, '_menu_item_menu_item_parent', '0'),
(307, 105, '_menu_item_object_id', '105'),
(308, 105, '_menu_item_object', 'custom'),
(309, 105, '_menu_item_target', ''),
(310, 105, '_menu_item_classes', 'a:1:{i:0;s:9:\"scrollTop\";}'),
(311, 105, '_menu_item_xfn', ''),
(312, 105, '_menu_item_url', '#secao-servicos');

-- --------------------------------------------------------

--
-- Estrutura para tabela `ph_posts`
--

CREATE TABLE `ph_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Fazendo dump de dados para tabela `ph_posts`
--

INSERT INTO `ph_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2020-04-29 14:34:38', '2020-04-29 17:34:38', '<!-- wp:paragraph -->\n<p>Boas-vindas ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!</p>\n<!-- /wp:paragraph -->', 'Olá, mundo!', '', 'publish', 'open', 'open', '', 'ola-mundo', '', '', '2020-04-29 14:34:38', '2020-04-29 17:34:38', '', 0, 'http://localhost/projetos/prohawk_site/?p=1', 0, 'post', '', 1),
(16, 1, '2020-04-30 11:23:16', '2020-04-30 14:23:16', '', 'Inicial', '', 'publish', 'closed', 'closed', '', 'inicial', '', '', '2020-04-30 11:23:16', '2020-04-30 14:23:16', '', 0, 'http://localhost/projetos/prohawk_site/?page_id=16', 0, 'page', '', 0),
(17, 1, '2020-04-30 11:23:16', '2020-04-30 14:23:16', '', 'Inicial', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2020-04-30 11:23:16', '2020-04-30 14:23:16', '', 16, 'http://localhost/projetos/prohawk_site/?p=17', 0, 'revision', '', 0),
(20, 1, '2020-04-30 11:38:52', '2020-04-30 14:38:52', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Projetos urbanos', '', 'publish', 'closed', 'closed', '', 'projetos-urbanos', '', '', '2020-05-20 12:58:27', '2020-05-20 15:58:27', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=areas_atuacao&#038;p=20', 0, 'areas_atuacao', '', 0),
(27, 1, '2020-04-30 11:39:14', '2020-04-30 14:39:14', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Grandes Obras', '', 'publish', 'closed', 'closed', '', 'grandes-obras', '', '', '2020-05-20 12:58:20', '2020-05-20 15:58:20', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=areas_atuacao&#038;p=27', 0, 'areas_atuacao', '', 0),
(28, 1, '2020-04-30 11:39:31', '2020-04-30 14:39:31', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Aeroportos', '', 'publish', 'closed', 'closed', '', 'aeroportos', '', '', '2020-05-20 12:58:14', '2020-05-20 15:58:14', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=areas_atuacao&#038;p=28', 0, 'areas_atuacao', '', 0),
(29, 1, '2020-04-30 11:39:43', '2020-04-30 14:39:43', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Portos', '', 'publish', 'closed', 'closed', '', 'portos', '', '', '2020-05-20 12:58:06', '2020-05-20 15:58:06', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=areas_atuacao&#038;p=29', 0, 'areas_atuacao', '', 0),
(30, 1, '2020-04-30 11:39:55', '2020-04-30 14:39:55', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Ferrovia', '', 'publish', 'closed', 'closed', '', 'ferrovia', '', '', '2020-05-20 12:58:00', '2020-05-20 15:58:00', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=areas_atuacao&#038;p=30', 0, 'areas_atuacao', '', 0),
(31, 1, '2020-04-30 11:40:08', '2020-04-30 14:40:08', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Rodovia', '', 'publish', 'closed', 'closed', '', 'rodovia', '', '', '2020-05-20 12:57:55', '2020-05-20 15:57:55', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=areas_atuacao&#038;p=31', 0, 'areas_atuacao', '', 0),
(32, 1, '2020-04-30 11:40:19', '2020-04-30 14:40:19', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Energia', '', 'publish', 'closed', 'closed', '', 'energia', '', '', '2020-05-20 12:57:47', '2020-05-20 15:57:47', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=areas_atuacao&#038;p=32', 0, 'areas_atuacao', '', 0),
(33, 1, '2020-04-30 11:40:32', '2020-04-30 14:40:32', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Saneamento', '', 'publish', 'closed', 'closed', '', 'saneamento', '', '', '2020-05-20 12:57:34', '2020-05-20 15:57:34', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=areas_atuacao&#038;p=33', 0, 'areas_atuacao', '', 0),
(34, 1, '2020-04-30 11:41:24', '2020-04-30 14:41:24', '', 'QR Code\'s.', '', 'publish', 'closed', 'closed', '', 'qr-codes', '', '', '2020-05-19 14:59:09', '2020-05-19 17:59:09', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=vantagem&#038;p=34', 9, 'vantagem', '', 0),
(35, 1, '2020-04-30 11:41:53', '2020-04-30 14:41:53', '', 'Altíssimo nível de detalhamento;', '', 'publish', 'closed', 'closed', '', 'altissimo-nivel-de-detalhamento', '', '', '2020-05-19 14:58:40', '2020-05-19 17:58:40', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=vantagem&#038;p=35', 4, 'vantagem', '', 0),
(36, 1, '2020-04-30 11:42:11', '2020-04-30 14:42:11', '', 'Arquivos físicos;', '', 'publish', 'closed', 'closed', '', 'arquivos-fisicos', '', '', '2020-05-19 14:59:04', '2020-05-19 17:59:04', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=vantagem&#038;p=36', 8, 'vantagem', '', 0),
(37, 1, '2020-04-30 11:42:31', '2020-04-30 14:42:31', '', 'Maior número de produtos gerados;', '', 'publish', 'closed', 'closed', '', 'maior-numero-de-produtos-gerados', '', '', '2020-05-19 14:58:33', '2020-05-19 17:58:33', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=vantagem&#038;p=37', 3, 'vantagem', '', 0),
(38, 1, '2020-04-30 11:42:48', '2020-04-30 14:42:48', '', 'Disponível em nuvem;', '', 'publish', 'closed', 'closed', '', 'disponivel-em-nuvem', '', '', '2020-05-19 14:58:58', '2020-05-19 17:58:58', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=vantagem&#038;p=38', 7, 'vantagem', '', 0),
(39, 1, '2020-04-30 11:43:14', '2020-04-30 14:43:14', '', 'Acesso a áreas de alta complexidade;', '', 'publish', 'closed', 'closed', '', 'acesso-a-areas-de-alta-complexidade', '', '', '2020-05-19 14:58:25', '2020-05-19 17:58:25', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=vantagem&#038;p=39', 2, 'vantagem', '', 0),
(40, 1, '2020-04-30 11:43:29', '2020-04-30 14:43:29', '', 'Entrega digital;', '', 'publish', 'closed', 'closed', '', 'entrega-digital', '', '', '2020-05-19 14:58:53', '2020-05-19 17:58:53', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=vantagem&#038;p=40', 6, 'vantagem', '', 0),
(41, 1, '2020-04-30 11:43:43', '2020-04-30 14:43:43', '', 'Menos profissionais em campo;', '', 'publish', 'closed', 'closed', '', 'menos-profissionais-em-campo', '', '', '2020-05-19 14:58:11', '2020-05-19 17:58:11', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=vantagem&#038;p=41', 1, 'vantagem', '', 0),
(42, 1, '2020-04-30 11:44:31', '2020-04-30 14:44:31', '', 'Economia de até 70% do valor do projeto convencional;', '', 'publish', 'closed', 'closed', '', 'economia-de-ate-70-do-valor-do-projeto-convencional', '', '', '2020-05-19 14:58:46', '2020-05-19 17:58:46', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=vantagem&#038;p=42', 5, 'vantagem', '', 0),
(43, 1, '2020-04-30 11:45:00', '2020-04-30 14:45:00', '', 'Redução de 90% do tempo em campo;', '', 'publish', 'closed', 'closed', '', 'reducao-de-90-do-tempo-em-campo', '', '', '2020-05-19 14:57:57', '2020-05-19 17:57:57', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=vantagem&#038;p=43', 0, 'vantagem', '', 0),
(44, 1, '2020-04-30 11:46:27', '2020-04-30 14:46:27', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate.', 'Projetos BIM.', '', 'publish', 'closed', 'closed', '', 'projetos-bim', '', '', '2020-05-19 15:09:16', '2020-05-19 18:09:16', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=servico&#038;p=44', 0, 'servico', '', 0),
(45, 1, '2020-04-30 11:46:44', '2020-04-30 14:46:44', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate.', 'Projeto executivo;', '', 'publish', 'closed', 'closed', '', 'projeto-executivo', '', '', '2020-05-19 15:09:11', '2020-05-19 18:09:11', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=servico&#038;p=45', 0, 'servico', '', 0),
(46, 1, '2020-04-30 11:47:35', '2020-04-30 14:47:35', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate.', 'Processos de gestão de ativos através de produtos gerados em processamento;', '', 'publish', 'closed', 'closed', '', 'processos-de-gestao-de-ativos-atraves-de-produtos-gerados-em-processamento', '', '', '2020-05-19 15:09:06', '2020-05-19 18:09:06', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=servico&#038;p=46', 0, 'servico', '', 0),
(47, 1, '2020-04-30 11:48:17', '2020-04-30 14:48:17', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate.', 'Acompanhamento e monitoramento precisos de obras;', '', 'publish', 'closed', 'closed', '', 'acompanhamento-e-monitoramento-precisos-de-obras', '', '', '2020-05-19 15:08:59', '2020-05-19 18:08:59', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=servico&#038;p=47', 0, 'servico', '', 0),
(48, 1, '2020-04-30 11:49:06', '2020-04-30 14:49:06', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate.', 'Cálculos volumétricos dos mais diversos formatos com precisão;', '', 'publish', 'closed', 'closed', '', 'calculos-volumetricos-dos-mais-diversos-formatos-com-precisao', '', '', '2020-05-19 15:08:54', '2020-05-19 18:08:54', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=servico&#038;p=48', 0, 'servico', '', 0),
(49, 1, '2020-04-30 11:49:46', '2020-04-30 14:49:46', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate.', 'Projetos altimétricos e planimétricos precisos e detalhados;', '', 'publish', 'closed', 'closed', '', 'projetos-altimetricos-e-planimetricos-precisos-e-detalhados', '', '', '2020-05-19 15:08:49', '2020-05-19 18:08:49', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=servico&#038;p=49', 0, 'servico', '', 0),
(50, 1, '2020-04-30 11:50:20', '2020-04-30 14:50:20', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate.', 'Ortomosaico Georreferenciado;', '', 'publish', 'closed', 'closed', '', 'ortomosaico-georreferenciado', '', '', '2020-05-19 15:08:44', '2020-05-19 18:08:44', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=servico&#038;p=50', 0, 'servico', '', 0),
(51, 1, '2020-04-30 11:50:32', '2020-04-30 14:50:32', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate.', 'Curvas de nível;', '', 'publish', 'closed', 'closed', '', 'curvas-de-nivel', '', '', '2020-05-19 15:08:40', '2020-05-19 18:08:40', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=servico&#038;p=51', 0, 'servico', '', 0),
(52, 1, '2020-04-30 11:50:57', '2020-04-30 14:50:57', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate.', 'Geração de modelo 3D;', '', 'publish', 'closed', 'closed', '', 'geracao-de-modelo-3d', '', '', '2020-05-19 15:08:34', '2020-05-19 18:08:34', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=servico&#038;p=52', 0, 'servico', '', 0),
(53, 1, '2020-04-30 11:51:32', '2020-04-30 14:51:32', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate.', 'Geração de Modelos Digitais de Terreno(MDT) e Superfície(MDS);', '', 'publish', 'closed', 'closed', '', 'geracao-de-modelos-digitais-de-terrenomdt-e-superficiemds', '', '', '2020-05-19 15:08:20', '2020-05-19 18:08:20', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=servico&#038;p=53', 0, 'servico', '', 0),
(54, 1, '2020-04-30 11:51:57', '2020-04-30 14:51:57', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate.', 'Nuvem de pontos altamente desificada;', '', 'publish', 'closed', 'closed', '', 'nuvem-de-pontos-altamente-desificada', '', '', '2020-05-19 15:08:12', '2020-05-19 18:08:12', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=servico&#038;p=54', 0, 'servico', '', 0),
(56, 1, '2020-04-30 14:44:32', '2020-04-30 17:44:32', '<label for=\"input-nome\">[text* input-nome id:input-nome placeholder \"Nome\"]</label><label for=\"input-email\">[email* input-email id:input-email placeholder \"Email\"]</label><label for=\"input-celular\">[tel* input-celular id:input-celular placeholder \"Celular\"]</label><div class=\"div-button-enviar\">[submit \"Enviar\"]</div>\n1\nProhawk \"Formulário site\"\nProhawk <dev@gran.ag>\ndev@gran.ag\nDe: [input-nome]\r\nEmail: [input-email]\r\nCelular: [input-celular]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Prohawk (http://localhost/projetos/prohawk_site)\nReply-To: [input-email]\n\n\n\n\nProhawk \"[your-subject]\"\nProhawk <dev@gran.ag>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Prohawk (http://localhost/projetos/prohawk_site)\nReply-To: dev@gran.ag\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Fale conosco', '', 'publish', 'closed', 'closed', '', 'fale-conosco', '', '', '2020-04-30 15:55:41', '2020-04-30 18:55:41', '', 0, 'http://localhost/projetos/prohawk_site/?post_type=wpcf7_contact_form&#038;p=56', 0, 'wpcf7_contact_form', '', 0),
(57, 1, '2020-04-30 15:01:55', '2020-04-30 18:01:55', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate.', 'Nuvem de pontos altamente desificada;', '', 'inherit', 'closed', 'closed', '', '54-autosave-v1', '', '', '2020-04-30 15:01:55', '2020-04-30 18:01:55', '', 54, 'http://localhost/projetos/prohawk_site/?p=57', 0, 'revision', '', 0),
(60, 1, '2020-05-19 11:33:10', '2020-05-19 14:33:10', '', 'banner', '', 'inherit', 'open', 'closed', '', 'banner', '', '', '2020-05-19 11:33:10', '2020-05-19 14:33:10', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/banner.png', 0, 'attachment', 'image/png', 0),
(61, 1, '2020-05-19 11:33:15', '2020-05-19 14:33:15', '', 'drone', '', 'inherit', 'open', 'closed', '', 'drone', '', '', '2020-05-19 11:33:15', '2020-05-19 14:33:15', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/drone.png', 0, 'attachment', 'image/png', 0),
(62, 1, '2020-05-19 11:33:17', '2020-05-19 14:33:17', '', 'droneservicos', '', 'inherit', 'open', 'closed', '', 'droneservicos', '', '', '2020-05-19 11:33:17', '2020-05-19 14:33:17', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/droneservicos.png', 0, 'attachment', 'image/png', 0),
(63, 1, '2020-05-19 11:33:19', '2020-05-19 14:33:19', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2020-05-19 11:33:19', '2020-05-19 14:33:19', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/logo.svg', 0, 'attachment', 'image/svg+xml', 0),
(64, 1, '2020-05-19 11:33:19', '2020-05-19 14:33:19', '', 'logofooter', '', 'inherit', 'open', 'closed', '', 'logofooter', '', '', '2020-05-19 11:33:19', '2020-05-19 14:33:19', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/logofooter.svg', 0, 'attachment', 'image/svg+xml', 0),
(65, 1, '2020-05-19 11:33:20', '2020-05-19 14:33:20', '', 'produtos', '', 'inherit', 'open', 'closed', '', 'produtos', '', '', '2020-05-19 11:33:20', '2020-05-19 14:33:20', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/produtos.png', 0, 'attachment', 'image/png', 0),
(66, 1, '2020-05-19 11:33:21', '2020-05-19 14:33:21', '', 'selo', '', 'inherit', 'open', 'closed', '', 'selo', '', '', '2020-05-19 11:33:21', '2020-05-19 14:33:21', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/selo.png', 0, 'attachment', 'image/png', 0),
(67, 1, '2020-05-19 11:33:21', '2020-05-19 14:33:21', '', 'selo1', '', 'inherit', 'open', 'closed', '', 'selo1', '', '', '2020-05-19 11:33:21', '2020-05-19 14:33:21', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/selo1.png', 0, 'attachment', 'image/png', 0),
(68, 1, '2020-05-19 11:33:22', '2020-05-19 14:33:22', '', 'selo2', '', 'inherit', 'open', 'closed', '', 'selo2', '', '', '2020-05-19 11:33:22', '2020-05-19 14:33:22', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/selo2.png', 0, 'attachment', 'image/png', 0),
(69, 1, '2020-05-19 11:33:23', '2020-05-19 14:33:23', '', 'selo3', '', 'inherit', 'open', 'closed', '', 'selo3', '', '', '2020-05-19 11:33:23', '2020-05-19 14:33:23', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/selo3.png', 0, 'attachment', 'image/png', 0),
(70, 1, '2020-05-19 11:33:23', '2020-05-19 14:33:23', '', 'selo4', '', 'inherit', 'open', 'closed', '', 'selo4', '', '', '2020-05-19 11:33:23', '2020-05-19 14:33:23', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/selo4.png', 0, 'attachment', 'image/png', 0),
(71, 1, '2020-05-19 11:33:24', '2020-05-19 14:33:24', '', 'servico1', '', 'inherit', 'open', 'closed', '', 'servico1', '', '', '2020-05-19 11:33:24', '2020-05-19 14:33:24', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/servico1.svg', 0, 'attachment', 'image/svg+xml', 0),
(72, 1, '2020-05-19 11:33:24', '2020-05-19 14:33:24', '', 'servico2', '', 'inherit', 'open', 'closed', '', 'servico2', '', '', '2020-05-19 11:33:24', '2020-05-19 14:33:24', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/servico2.svg', 0, 'attachment', 'image/svg+xml', 0),
(73, 1, '2020-05-19 11:33:24', '2020-05-19 14:33:24', '', 'servico3', '', 'inherit', 'open', 'closed', '', 'servico3', '', '', '2020-05-19 11:33:24', '2020-05-19 14:33:24', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/servico3.svg', 0, 'attachment', 'image/svg+xml', 0),
(74, 1, '2020-05-19 11:33:25', '2020-05-19 14:33:25', '', 'servico4', '', 'inherit', 'open', 'closed', '', 'servico4', '', '', '2020-05-19 11:33:25', '2020-05-19 14:33:25', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/servico4.svg', 0, 'attachment', 'image/svg+xml', 0),
(75, 1, '2020-05-19 11:33:25', '2020-05-19 14:33:25', '', 'servico5', '', 'inherit', 'open', 'closed', '', 'servico5', '', '', '2020-05-19 11:33:25', '2020-05-19 14:33:25', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/servico5.svg', 0, 'attachment', 'image/svg+xml', 0),
(76, 1, '2020-05-19 11:33:25', '2020-05-19 14:33:25', '', 'servico6', '', 'inherit', 'open', 'closed', '', 'servico6', '', '', '2020-05-19 11:33:25', '2020-05-19 14:33:25', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/servico6.svg', 0, 'attachment', 'image/svg+xml', 0),
(77, 1, '2020-05-19 11:33:26', '2020-05-19 14:33:26', '', 'servico7', '', 'inherit', 'open', 'closed', '', 'servico7', '', '', '2020-05-19 11:33:26', '2020-05-19 14:33:26', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/servico7.svg', 0, 'attachment', 'image/svg+xml', 0),
(78, 1, '2020-05-19 11:33:26', '2020-05-19 14:33:26', '', 'servico8', '', 'inherit', 'open', 'closed', '', 'servico8', '', '', '2020-05-19 11:33:26', '2020-05-19 14:33:26', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/servico8.svg', 0, 'attachment', 'image/svg+xml', 0),
(80, 1, '2020-05-19 11:33:29', '2020-05-19 14:33:29', '', 'vantagem1', '', 'inherit', 'open', 'closed', '', 'vantagem1', '', '', '2020-05-19 11:33:29', '2020-05-19 14:33:29', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/vantagem1.svg', 0, 'attachment', 'image/svg+xml', 0),
(81, 1, '2020-05-19 11:33:29', '2020-05-19 14:33:29', '', 'vantagem2', '', 'inherit', 'open', 'closed', '', 'vantagem2', '', '', '2020-05-19 11:33:29', '2020-05-19 14:33:29', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/vantagem2.svg', 0, 'attachment', 'image/svg+xml', 0),
(82, 1, '2020-05-19 11:33:29', '2020-05-19 14:33:29', '', 'vantagem3', '', 'inherit', 'open', 'closed', '', 'vantagem3', '', '', '2020-05-19 11:33:29', '2020-05-19 14:33:29', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/vantagem3.svg', 0, 'attachment', 'image/svg+xml', 0),
(83, 1, '2020-05-19 11:33:30', '2020-05-19 14:33:30', '', 'vantagem4', '', 'inherit', 'open', 'closed', '', 'vantagem4', '', '', '2020-05-19 11:33:30', '2020-05-19 14:33:30', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/vantagem4.svg', 0, 'attachment', 'image/svg+xml', 0),
(84, 1, '2020-05-19 11:33:30', '2020-05-19 14:33:30', '', 'vantagem5', '', 'inherit', 'open', 'closed', '', 'vantagem5', '', '', '2020-05-19 11:33:30', '2020-05-19 14:33:30', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/vantagem5.svg', 0, 'attachment', 'image/svg+xml', 0),
(85, 1, '2020-05-19 11:33:30', '2020-05-19 14:33:30', '', 'vantagem6', '', 'inherit', 'open', 'closed', '', 'vantagem6', '', '', '2020-05-19 11:33:30', '2020-05-19 14:33:30', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/vantagem6.svg', 0, 'attachment', 'image/svg+xml', 0),
(86, 1, '2020-05-19 11:33:31', '2020-05-19 14:33:31', '', 'vantagem7', '', 'inherit', 'open', 'closed', '', 'vantagem7', '', '', '2020-05-19 11:33:31', '2020-05-19 14:33:31', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/vantagem7.svg', 0, 'attachment', 'image/svg+xml', 0),
(87, 1, '2020-05-19 11:33:31', '2020-05-19 14:33:31', '', 'vantagem8', '', 'inherit', 'open', 'closed', '', 'vantagem8', '', '', '2020-05-19 11:33:31', '2020-05-19 14:33:31', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/vantagem8.svg', 0, 'attachment', 'image/svg+xml', 0),
(88, 1, '2020-05-19 11:33:32', '2020-05-19 14:33:32', '', 'vantagem9', '', 'inherit', 'open', 'closed', '', 'vantagem9', '', '', '2020-05-19 11:33:32', '2020-05-19 14:33:32', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/vantagem9.svg', 0, 'attachment', 'image/svg+xml', 0),
(89, 1, '2020-05-19 11:33:32', '2020-05-19 14:33:32', '', 'vantagem10', '', 'inherit', 'open', 'closed', '', 'vantagem10', '', '', '2020-05-19 11:33:32', '2020-05-19 14:33:32', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/vantagem10.svg', 0, 'attachment', 'image/svg+xml', 0),
(90, 1, '2020-05-19 11:54:11', '2020-05-19 14:54:11', '', 'sobre', '', 'inherit', 'open', 'closed', '', 'sobre-2', '', '', '2020-05-19 11:54:11', '2020-05-19 14:54:11', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/sobre-1.png', 0, 'attachment', 'image/png', 0),
(91, 1, '2020-05-19 15:08:08', '2020-05-19 18:08:08', '', 'produtos', '', 'inherit', 'open', 'closed', '', 'produtos-2', '', '', '2020-05-19 15:08:08', '2020-05-19 18:08:08', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/produtos-1.png', 0, 'attachment', 'image/png', 0),
(92, 1, '2020-05-19 15:29:33', '2020-05-19 18:29:33', '', 'logofooter', '', 'inherit', 'open', 'closed', '', 'logofooter-2', '', '', '2020-05-19 15:29:33', '2020-05-19 18:29:33', '', 0, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/05/logofooter.png', 0, 'attachment', 'image/png', 0),
(93, 1, '2020-05-20 12:57:16', '2020-05-20 15:57:16', '', 'servico1b', '', 'inherit', 'open', 'closed', '', 'servico1b', '', '', '2020-05-20 12:57:16', '2020-05-20 15:57:16', '', 33, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/04/servico1b.svg', 0, 'attachment', 'image/svg+xml', 0),
(94, 1, '2020-05-20 12:57:17', '2020-05-20 15:57:17', '', 'servico2b', '', 'inherit', 'open', 'closed', '', 'servico2b', '', '', '2020-05-20 12:57:17', '2020-05-20 15:57:17', '', 33, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/04/servico2b.svg', 0, 'attachment', 'image/svg+xml', 0),
(95, 1, '2020-05-20 12:57:17', '2020-05-20 15:57:17', '', 'servico3b', '', 'inherit', 'open', 'closed', '', 'servico3b', '', '', '2020-05-20 12:57:17', '2020-05-20 15:57:17', '', 33, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/04/servico3b.svg', 0, 'attachment', 'image/svg+xml', 0),
(96, 1, '2020-05-20 12:57:17', '2020-05-20 15:57:17', '', 'servico4b', '', 'inherit', 'open', 'closed', '', 'servico4b', '', '', '2020-05-20 12:57:17', '2020-05-20 15:57:17', '', 33, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/04/servico4b.svg', 0, 'attachment', 'image/svg+xml', 0),
(97, 1, '2020-05-20 12:57:18', '2020-05-20 15:57:18', '', 'servico5b', '', 'inherit', 'open', 'closed', '', 'servico5b', '', '', '2020-05-20 12:57:18', '2020-05-20 15:57:18', '', 33, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/04/servico5b.svg', 0, 'attachment', 'image/svg+xml', 0),
(98, 1, '2020-05-20 12:57:18', '2020-05-20 15:57:18', '', 'servico6b', '', 'inherit', 'open', 'closed', '', 'servico6b', '', '', '2020-05-20 12:57:18', '2020-05-20 15:57:18', '', 33, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/04/servico6b.svg', 0, 'attachment', 'image/svg+xml', 0),
(99, 1, '2020-05-20 12:57:19', '2020-05-20 15:57:19', '', 'servico7b', '', 'inherit', 'open', 'closed', '', 'servico7b', '', '', '2020-05-20 12:57:19', '2020-05-20 15:57:19', '', 33, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/04/servico7b.svg', 0, 'attachment', 'image/svg+xml', 0),
(100, 1, '2020-05-20 12:57:19', '2020-05-20 15:57:19', '', 'servico8b', '', 'inherit', 'open', 'closed', '', 'servico8b', '', '', '2020-05-20 12:57:19', '2020-05-20 15:57:19', '', 33, 'http://localhost/projetos/prohawk_site/wp-content/uploads/2020/04/servico8b.svg', 0, 'attachment', 'image/svg+xml', 0),
(101, 1, '2020-05-20 16:22:16', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-05-20 16:22:16', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/prohawk_site/?p=101', 1, 'nav_menu_item', '', 0),
(102, 1, '2020-05-20 16:24:30', '2020-05-20 19:24:30', '', 'Sobre', '', 'publish', 'closed', 'closed', '', 'sobre', '', '', '2020-05-20 16:24:50', '2020-05-20 19:24:50', '', 0, 'http://localhost/projetos/prohawk_site/?p=102', 1, 'nav_menu_item', '', 0),
(103, 1, '2020-05-20 16:24:31', '2020-05-20 19:24:31', '', 'Áreas de atuação', '', 'publish', 'closed', 'closed', '', 'areas-de-atuacao', '', '', '2020-05-20 16:24:50', '2020-05-20 19:24:50', '', 0, 'http://localhost/projetos/prohawk_site/?p=103', 2, 'nav_menu_item', '', 0),
(104, 1, '2020-05-20 16:24:32', '2020-05-20 19:24:32', '', 'Benefícios e vantagens', '', 'publish', 'closed', 'closed', '', 'beneficios-e-vantagens', '', '', '2020-05-20 16:24:50', '2020-05-20 19:24:50', '', 0, 'http://localhost/projetos/prohawk_site/?p=104', 3, 'nav_menu_item', '', 0),
(105, 1, '2020-05-20 16:24:33', '2020-05-20 19:24:33', '', 'Serviços', '', 'publish', 'closed', 'closed', '', 'servicos', '', '', '2020-05-20 16:24:50', '2020-05-20 19:24:50', '', 0, 'http://localhost/projetos/prohawk_site/?p=105', 4, 'nav_menu_item', '', 0),
(106, 1, '2020-05-26 14:45:33', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2020-05-26 14:45:33', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/prohawk_site/?p=106', 0, 'post', '', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `ph_termmeta`
--

CREATE TABLE `ph_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `ph_terms`
--

CREATE TABLE `ph_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Fazendo dump de dados para tabela `ph_terms`
--

INSERT INTO `ph_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sem categoria', 'sem-categoria', 0),
(2, 'Menu principal', 'menu-principal', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `ph_term_relationships`
--

CREATE TABLE `ph_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Fazendo dump de dados para tabela `ph_term_relationships`
--

INSERT INTO `ph_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(102, 2, 0),
(103, 2, 0),
(104, 2, 0),
(105, 2, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `ph_term_taxonomy`
--

CREATE TABLE `ph_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Fazendo dump de dados para tabela `ph_term_taxonomy`
--

INSERT INTO `ph_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 4);

-- --------------------------------------------------------

--
-- Estrutura para tabela `ph_usermeta`
--

CREATE TABLE `ph_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Fazendo dump de dados para tabela `ph_usermeta`
--

INSERT INTO `ph_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'prohawk'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'ph_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'ph_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(17, 1, 'ph_dashboard_quick_press_last_post_id', '106'),
(18, 1, 'session_tokens', 'a:2:{s:64:\"8e6066d1e5a9e1705a48f4d120d718d1933bde8541449d88661876920d16484a\";a:4:{s:10:\"expiration\";i:1590684581;s:2:\"ip\";s:15:\"191.179.227.206\";s:2:\"ua\";s:105:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36\";s:5:\"login\";i:1590511781;}s:64:\"0602f7ad484aab4db12bcf909c36c03d9bc33fd020d8a42abacc9f8c2e1d4b19\";a:4:{s:10:\"expiration\";i:1590713005;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:105:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36\";s:5:\"login\";i:1590540205;}}'),
(19, 1, 'ph_r_tru_u_x', 'a:2:{s:2:\"id\";i:0;s:7:\"expires\";i:1588276783;}'),
(20, 1, 'ph_user-settings', 'libraryContent=browse&editor=tinymce'),
(21, 1, 'ph_user-settings-time', '1588260662'),
(22, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:13:\"191.179.227.0\";}'),
(23, 1, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:\"link-target\";i:1;s:15:\"title-attribute\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";}'),
(24, 1, 'metaboxhidden_nav-menus', 'a:4:{i:0;s:27:\"add-post-type-areas_atuacao\";i:1;s:22:\"add-post-type-vantagem\";i:2;s:21:\"add-post-type-servico\";i:3;s:12:\"add-post_tag\";}'),
(25, 1, 'nav_menu_recently_edited', '2');

-- --------------------------------------------------------

--
-- Estrutura para tabela `ph_users`
--

CREATE TABLE `ph_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Fazendo dump de dados para tabela `ph_users`
--

INSERT INTO `ph_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'prohawk', '$P$BXuCPfJRcEkvZt2N4HUHRwKM1CDOP81', 'prohawk', 'dev@gran.ag', 'http://localhost/projetos/prohawk_site', '2020-04-29 17:34:34', '', 0, 'prohawk');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `ph_commentmeta`
--
ALTER TABLE `ph_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Índices de tabela `ph_comments`
--
ALTER TABLE `ph_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Índices de tabela `ph_links`
--
ALTER TABLE `ph_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Índices de tabela `ph_options`
--
ALTER TABLE `ph_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Índices de tabela `ph_postmeta`
--
ALTER TABLE `ph_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Índices de tabela `ph_posts`
--
ALTER TABLE `ph_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Índices de tabela `ph_termmeta`
--
ALTER TABLE `ph_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Índices de tabela `ph_terms`
--
ALTER TABLE `ph_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Índices de tabela `ph_term_relationships`
--
ALTER TABLE `ph_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Índices de tabela `ph_term_taxonomy`
--
ALTER TABLE `ph_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Índices de tabela `ph_usermeta`
--
ALTER TABLE `ph_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Índices de tabela `ph_users`
--
ALTER TABLE `ph_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `ph_commentmeta`
--
ALTER TABLE `ph_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `ph_comments`
--
ALTER TABLE `ph_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `ph_links`
--
ALTER TABLE `ph_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `ph_options`
--
ALTER TABLE `ph_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=614;
--
-- AUTO_INCREMENT de tabela `ph_postmeta`
--
ALTER TABLE `ph_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=313;
--
-- AUTO_INCREMENT de tabela `ph_posts`
--
ALTER TABLE `ph_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;
--
-- AUTO_INCREMENT de tabela `ph_termmeta`
--
ALTER TABLE `ph_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `ph_terms`
--
ALTER TABLE `ph_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `ph_term_taxonomy`
--
ALTER TABLE `ph_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `ph_usermeta`
--
ALTER TABLE `ph_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT de tabela `ph_users`
--
ALTER TABLE `ph_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
