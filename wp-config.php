<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'projetos_prohawk_site' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '24368' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ':R1pG2jq9T}(rIjlU6:L/C7f&A*>3keK7C2KixqZra-{pAK( G#J&t(nOu~<JhB5' );
define( 'SECURE_AUTH_KEY',  'iKd.VS8Y6wuyHN[=<Tk;<9#=rbvvEB4iTEeZ|~?PopDIYNT}ZH7jo21z!fQ+EA+>' );
define( 'LOGGED_IN_KEY',    '#AW(q~4H-PoM9S2LDWpt_cD9f|l9j6~n6XKQ)/J33/,7(~XFEL*BY)!yko90ImN,' );
define( 'NONCE_KEY',        '[Nfxm7iMDbn*NayHtH-M`?3C8^!9xBVNz>dVVP56-+odq4Crc<.ug*e OF*?KqiZ' );
define( 'AUTH_SALT',        '#RdBnRV]9P9j/(.;6vg9j/{yHL+>=8|?WzcG_sb%.^/lVWzf^V;GMW:dIh81Z>KH' );
define( 'SECURE_AUTH_SALT', 'L{{(}ixm74mkDFRtvUGnvO`h)6i@g3,5$eM_[|26+hctpt`L~$$p=bWb6#B&(,?%' );
define( 'LOGGED_IN_SALT',   'rA`0s1:>a$%@JYn>*>B;Z}!.{[/39*6fM%+KXMk#E8p)E|Mt^7}[RjW:#/r{hN_o' );
define( 'NONCE_SALT',       '%[z+P~>u)(2cr8.z5G$.DdWO_@d]rJLsRM+t>dI:MAdy+a5Nn|^pMA@LkgC6_=[M' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'ph_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';

define('FS_METHOD','direct');