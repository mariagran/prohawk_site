<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Prohawk
 */

global $configuracao;

?>

<footer>
	<div class="large-container">
		<div class="row">
			<div class="col-md-5">
				<figure class="logo-footer">
					<img src="<?php echo $configuracao['opt_logo_footer']['url']; ?>" alt="Logo Prohawk">
					<figcaption class="hidden">Logo Prohawk</figcaption>
				</figure>
				<ul class="selos-prohawk">

					<?php $selosId = $configuracao['opt_selos'];
					$selosId = explode(",", $selosId);
					foreach($selosId as $selo): $imageUrl = wp_get_attachment_image_src($selo)[0]; ?>
					<li><img src="<?php echo $imageUrl; ?>" alt="Selo Prohawk"></li>
					<?php endforeach; ?>

				</ul>
			</div>
			<div class="col-md-2">
				<div class="menu-footer">
					<h3 class="titulo">Áreas de atuação</h3>
					<nav>
						<ul>
							<li class="scrollTop">
								<a href="#secao-areas-atuacao">Saneamento</a>
							</li>
							<li class="scrollTop">
								<a href="#secao-areas-atuacao">Energia</a>
							</li>
							<li class="scrollTop">
								<a href="#secao-areas-atuacao">Infraestrutura</a>
							</li>
							<li class="scrollTop">
								<a href="#secao-areas-atuacao">Construção Civil</a>
							</li>
							<li class="scrollTop">
								<a href="#secao-areas-atuacao">Grandes Obras</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
			<div class="col-md-3">
				<div class="menu-footer">
					<h3 class="titulo">Entre em Contato</h3>
					<nav>
						<span><?php echo $configuracao['opt_email']; ?></span>
					</nav>
				</div>
				<div class="menu-footer">
					<h3 class="titulo">WhatsApp</h3>
					<nav>
						<a href="https://api.whatsapp.com/send?phone=<?php echo $configuracao['opt_whatsapp']; ?>&text=Ol%C3%A1!" target="_blank"><?php echo $configuracao['opt_whatsapp_texto']; ?></a>
					</nav>
				</div>
			</div>
			<div class="col-md-2">
				<div class="menu-footer">
					<h3 class="titulo">Social Media</h3>
					<nav>
						<a href="<?php echo $configuracao['opt_facebook']; ?>" target="_blank">Facebook</a>
						<a href="<?php echo $configuracao['opt_instagram']; ?>" target="_blank">Instagram</a>
						<a href="<?php echo $configuracao['opt_youtube']; ?>" target="_blank">Youtube</a>
						<a href="<?php echo $configuracao['opt_linkedin']; ?>" target="_blank">LinkedIn</a>
					</nav>
				</div>
			</div>
		</div>
	</div>
	<div class="copyright">
		<div class="large-container">
			<div class="row">
				<div class="col-md-6 desktop-element">
					<p><?php echo $configuracao['opt_titulo_copyright'] ?></p>
				</div>
				<div class="col-md-6">
					<p><a href="https://goo.gl/maps/ioCufQYR3sQNZUDw8" target="_blank"><?php echo $configuracao['opt_endereco_copyright'] ?></a></p>
				</div>
				<div class="col-md-6 mobile-element">
					<p><?php echo $configuracao['opt_titulo_copyright'] ?></p>
				</div>
			</div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>