(function(){

	$(document).ready(function(){

		/* 
		** Carrossel de serviços
		*/
		$('.carrossel-servicos').owlCarousel({
			items: 1,
			dots: false,
			nav: true,
			loop: false,
			lazyLoad: false,
			mouseDrag: false,
			touchDrag: false,
			animateIn: 'fadeIn',
			animateOut: 'fadeOut',
			smartSpeed: 450,
		});

		/* 
		** Função serviços: mostrar conteúdo de cada produto no clique
		** início
		*/
		/*-- desktop e mobile: mostra o conteúdo de cada serviço --*/
		let liProdutos = document.querySelectorAll('.pg-inicial .secao-servicos .produtos-gerados ul li p');
		let produtos = document.querySelectorAll('.pg-inicial .secao-servicos .conteudo-produtos-gerados');

		liProdutos.forEach(function(item){
			item.addEventListener('click', function(){
				let dataId = this.getAttribute('data-id');

				liProdutos.forEach(function(item){
					item.classList.remove('produto-ativo');
				});
				this.classList.add('produto-ativo');

				produtos.forEach(function(item){
					item.classList.remove('conteudo-produtos-ativo');
				});

				let produto = produtos[dataId];
				produto.classList.add('conteudo-produtos-ativo');

				if(screen.width <= 768){
					ulProdutos.classList.remove('abrir-menu-produtos-gerados');
					isMenuProdutosOpen = false;
					spanProdutoAtivo.innerText = item.innerText;
				}
			});
		});
		document.querySelector('.pg-inicial .secao-servicos .produtos-gerados ul li p[data-id="0"]').click();
		
		/*-- mobile: abre e fecha o menu mobile --*/
		let spanProdutoAtivo = document.querySelector('.pg-inicial .secao-servicos .produtos-gerados .menu-produtos-gerados .span-produto-ativo');
		let ulProdutos = document.querySelector('.pg-inicial .secao-servicos .produtos-gerados .menu-produtos-gerados ul');

		let isMenuProdutosOpen = false;

		spanProdutoAtivo.addEventListener('click', function(){
			if(!isMenuProdutosOpen){
				ulProdutos.classList.add('abrir-menu-produtos-gerados');
				isMenuProdutosOpen = true;
			} else{
				ulProdutos.classList.remove('abrir-menu-produtos-gerados');
				isMenuProdutosOpen = false;
			}
		});
		/* 
		** Função serviços
		** fim
		*/
		
		/* 
		** Função áreas de atuação: abrir ou fechar formulário de contato
		** início
		*/
		let contentFormulario = document.getElementById('formularioFaleConosco');
		contentFormulario.remove();
		$('.pg-inicial .secao-areas-atuacao article .areas-atuacao li .span-arrow img').click(function(){
			if(screen.width > 670){
				let thisLi = $(this).parent().parent();

				$('.pg-inicial .secao-areas-atuacao article .areas-atuacao li').removeClass('active-li');
				thisLi.addClass('active-li');

				$('.pg-inicial .secao-areas-atuacao article .areas-atuacao li .container-formulario').removeClass('open-formulario');

				let containerFormulario = thisLi.children('.container-formulario');
				containerFormulario.append(contentFormulario);

				containerFormulario.addClass('open-formulario');
				setTimeout(function(){
					contentFormulario.classList.add('show-formulario');
				}, 250);
			}
		});

		$('.pg-inicial .secao-areas-atuacao article .areas-atuacao li').mouseover(function(){
			if(screen.width > 500){
				$('.secao-areas-atuacao #formularioFaleConosco .fechar-formulario img').click(function(){
					let containerFormulario = $(this).parent().parent().parent();

					contentFormulario.classList.remove('show-formulario');
					setTimeout(function(){
						containerFormulario.removeClass('open-formulario');
						$('.pg-inicial .secao-areas-atuacao article .areas-atuacao li').removeClass('active-li');
						contentFormulario.remove();
					}, 250);

				});
			}
		});
		/* 
		** Função áreas de atuação: abrir ou fechar formulário de contato
		** fim
		*/

		$('.scrollTop a').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
			
		});

		/* 
		** Função abrir e fechar pop up
		** início
		*/
		let body = document.querySelector('body');

		let buttonSaibaMais = document.querySelectorAll('.pg-inicial .secao-servicos .conteudo-produtos-gerados article .button-saiba-mais span, footer .menu-footer nav span');
		let popUp = document.querySelector('.div-pop-up-formulario');
		let popUpFormulario = document.querySelector('.div-pop-up-formulario .container-pop-up .container-formulario');

		buttonSaibaMais.forEach(function(item){
			item.addEventListener('click', function(event){
				event.preventDefault();
				popUp.classList.add('abrir-pop-up');

				popUpFormulario.append(contentFormulario);
				contentFormulario.classList.add('show-formulario');

				body.classList.add('stopScroll');
			});
		});
		popUp.addEventListener('mouseover', function(){
			let closePopUP = document.querySelector('.div-pop-up-formulario.abrir-pop-up #formularioFaleConosco .fechar-formulario img');

			closePopUP.addEventListener('click', function(){
				popUp.classList.remove('abrir-pop-up');

				setTimeout(function(){
					contentFormulario.classList.remove('show-formulario');
					contentFormulario.remove();
					body.classList.remove('stopScroll');
				}, 300);
			});
		});
		/* 
		** Função abrir e fechar pop up
		** fim
		*/

		$('header .open-menu-mobile').click(function(){
			$('header .navbar').addClass('opened-menu');
			$('body').addClass('stopScroll');
		});
		$('header .navbar .close-menu-mobile').click(function(){
			$('header .navbar').removeClass('opened-menu');
			$('body').removeClass('stopScroll');
		});
		$('header a').click(function(){
			$('header .navbar').removeClass('opened-menu');
			$('body').removeClass('stopScroll');
		});

		let sobreProhawk = document.querySelectorAll('.prohawk li');
		let sobreProhawkText = [];
		sobreProhawk.forEach(function(item){
			sobreProhawkText.push(item.innerText);
		});

		setTimeout(function(){
			var typedDestaque = new Typed('.pg-inicial .secao-destaque .conteudo-destaque article .titulo span', {
				strings: sobreProhawkText,
				typeSpeed: 95,
				backDelay: 2500,
				backSpeed: 30,
				showCursor: false,
				loop: true,
				smartBackspace: false,
				onStringTyped: (arrayPos, self) => {
					setTimeout(function(){
						document.querySelector('.pg-inicial .secao-destaque .conteudo-destaque article .titulo span').classList.add('my-fade-out');
						document.querySelector('.pg-inicial .secao-destaque .conteudo-destaque article .titulo span').classList.remove('my-translate-up');

						setTimeout(function(){
							document.querySelector('.pg-inicial .secao-destaque .conteudo-destaque article .titulo span').classList.add('my-translate-down');
						}, 350);
					}, 2300);
				},
				preStringTyped: (arrayPos, self) => {
					document.querySelector('.pg-inicial .secao-destaque .conteudo-destaque article .titulo span').classList.remove('my-fade-out');
					document.querySelector('.pg-inicial .secao-destaque .conteudo-destaque article .titulo span').classList.remove('my-translate-down');
					document.querySelector('.pg-inicial .secao-destaque .conteudo-destaque article .titulo span').classList.add('my-translate-up');
				},
			});
			var typedVantagens = new Typed('.pg-inicial .secao-vantagens article .titulos-vantagens h2.titulo span', {
				strings: sobreProhawkText,
				typeSpeed: 95,
				backDelay: 2500,
				backSpeed: 30,
				showCursor: false,
				loop: true,
				smartBackspace: false,
				onStringTyped: (arrayPos, self) => {
					setTimeout(function(){
						document.querySelector('.pg-inicial .secao-vantagens article .titulos-vantagens h2.titulo span').classList.add('my-fade-out');
						document.querySelector('.pg-inicial .secao-vantagens article .titulos-vantagens h2.titulo span').classList.remove('my-translate-up');

						setTimeout(function(){
							document.querySelector('.pg-inicial .secao-vantagens article .titulos-vantagens h2.titulo span').classList.add('my-translate-down');
						}, 350);
					}, 2300);
				},
				preStringTyped: (arrayPos, self) => {
					document.querySelector('.pg-inicial .secao-vantagens article .titulos-vantagens h2.titulo span').classList.remove('my-fade-out');
					document.querySelector('.pg-inicial .secao-vantagens article .titulos-vantagens h2.titulo span').classList.remove('my-translate-down');
					document.querySelector('.pg-inicial .secao-vantagens article .titulos-vantagens h2.titulo span').classList.add('my-translate-up');
				},
			});
		}, 300);

		// let element = document.querySelector('.pg-inicial .secao-sobre .titulo-sobre-prohawk .titulos-sobre');

		// window.addEventListener('scroll', function(){
		// 	element.classList.add('show-translate-y');
		// });


		let alturaTela = $(window).height(); // Pega altura da tela
		let ajuste = alturaTela / 3; // O elemento vai ativar ao chegar no meio da tela, pode ser um numero customizado também
		let ajustex = alturaTela / 2; // O elemento vai ativar ao chegar no meio da tela, pode ser um numero customizado também
		let ajustexLi = alturaTela / 4; // O elemento vai ativar ao chegar no meio da tela, pode ser um numero customizado também

		$(window).scroll(function(event){ // Detecta o scroll do mouse
			$('.scroll-item-y').each(function(i){ // Faz um foreach com os elementos que deseja identificar
			
				let status = $(this).hasClass('show-translate-y'); // Usado para identificar se o elemento está ativo.
				
				if (!status) { // Se o elemento NÃO ! estiver ativo
					let elementoTop = $(this).offset().top + ajuste; // Pega a posição do elemento no eixo Y (vertical) e soma com o ajuste
					let elementoBottom = elementoTop + $(this).outerHeight(); // Soma a posição do elemento com a altura total dele
					let viewportTop = $(window).scrollTop(); // Pega a posição atual do scroll no eixo Y (vertical)
					let viewportBottom = viewportTop + alturaTela; // Soma a posição atual do scroll com a altura da tela.
					
					if (elementoBottom > viewportTop && elementoTop < viewportBottom) {
						// $('.scroll-item').removeClass('show-translate-y'); // Remove a classe de todos os elementos
						$(this).addClass('show-translate-y'); // Adiciona a classe ao elemento atual
					}
				}

			});
		});

		$(window).scroll(function(event){ // Detecta o scroll do mouse
			$('.scroll-item-x').each(function(i){ // Faz um foreach com os elementos que deseja identificar
			
				let status = $(this).hasClass('show-translate-x'); // Usado para identificar se o elemento está ativo.
				
				if (!status) { // Se o elemento NÃO ! estiver ativo
					let elementoTop = $(this).offset().top + ajustex; // Pega a posição do elemento no eixo Y (vertical) e soma com o ajuste
					let elementoBottom = elementoTop + $(this).outerHeight(); // Soma a posição do elemento com a altura total dele
					let viewportTop = $(window).scrollTop(); // Pega a posição atual do scroll no eixo Y (vertical)
					let viewportBottom = viewportTop + alturaTela; // Soma a posição atual do scroll com a altura da tela.
					
					if (elementoBottom > viewportTop && elementoTop < viewportBottom) {
						// $('.scroll-item').removeClass('show-translate-y'); // Remove a classe de todos os elementos
						$(this).addClass('show-translate-x'); // Adiciona a classe ao elemento atual
					}
				}

			});
		});

		$(window).scroll(function(event){ // Detecta o scroll do mouse
			$('li.scroll-item-x').each(function(i){ // Faz um foreach com os elementos que deseja identificar
			
				let status = $(this).hasClass('show-translate-x'); // Usado para identificar se o elemento está ativo.
				
				if (!status) { // Se o elemento NÃO ! estiver ativo
					let elementoTop = $(this).offset().top + ajustexLi; // Pega a posição do elemento no eixo Y (vertical) e soma com o ajuste
					let elementoBottom = elementoTop + $(this).outerHeight(); // Soma a posição do elemento com a altura total dele
					let viewportTop = $(window).scrollTop(); // Pega a posição atual do scroll no eixo Y (vertical)
					let viewportBottom = viewportTop + alturaTela; // Soma a posição atual do scroll com a altura da tela.
					
					if (elementoBottom > viewportTop && elementoTop < viewportBottom) {
						// $('.scroll-item').removeClass('show-translate-y'); // Remove a classe de todos os elementos
						$(this).addClass('show-translate-x'); // Adiciona a classe ao elemento atual
						setTimeout(function(){
							$('.pg-inicial .secao-areas-atuacao article .areas-atuacao li').addClass('correct-transition');
						}, 2000);
					}
				}

			});
		});

		// let counter = 0;
		// $(window).scroll(function(event){ // Detecta o scroll do mouse
		// 	let imagemDrone = $('.pg-inicial .secao-areas-atuacao article .topo-areas-atuacao .topo-areas-atuacao-imagem.desktop-element img');
		// 	imagemDrone.each(function(i){ // Faz um foreach com os elementos que deseja identificar
			
		// 		let status = $(this).hasClass('translate-drone'); // Usado para identificar se o elemento está ativo.
				
		// 		if (status == false && counter < 1) { // Se o elemento NÃO ! estiver ativo
		// 			let elementoTop = $(this).offset().top + ajustex; // Pega a posição do elemento no eixo Y (vertical) e soma com o ajuste
		// 			let elementoBottom = elementoTop + $(this).outerHeight(); // Soma a posição do elemento com a altura total dele
		// 			let viewportTop = $(window).scrollTop(); // Pega a posição atual do scroll no eixo Y (vertical)
		// 			let viewportBottom = viewportTop + alturaTela; // Soma a posição atual do scroll com a altura da tela.

		// 			let myThis = $(this);
					
		// 			if (elementoBottom > viewportTop && elementoTop < viewportBottom) {
		// 				$(this).addClass('translate-drone'); // Adiciona a classe ao elemento atual

		// 				setTimeout(function(){
		// 					myThis.removeClass('translate-drone');
		// 				}, 900);
		// 			}
		// 			counter++;
		// 		}
		// 	});
		// });

		// window.setInterval(function(){
		// 	$('.pg-inicial .secao-areas-atuacao article .topo-areas-atuacao .topo-areas-atuacao-imagem img').toggleClass('translate-drone');
		// }, 1000);


		$('.pg-inicial .secao-sobre .titulo-sobre-prohawk .conteudo-sobre .texto-sobre .button-saiba-mais span').click(function(){
			$('.div-pop-up-video-conheca-prohawk').addClass('open-pop-up-video');
		});
		$('.div-pop-up-video-conheca-prohawk .conteudo-pop-up-video .close-pop-up-video').click(function(){
			$('.div-pop-up-video-conheca-prohawk').removeClass('open-pop-up-video');
		});


	});

}());