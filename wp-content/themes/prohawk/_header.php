<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Prohawk
 */

global $configuracao;

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

	<!-- META -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<header>
		<div class="large-container">
			<div class="row">
				<!-- LOGO -->
				<div class="col-sm-3">
					<a class="logo" href="#">
						<figure>
							<img src="<?php echo $configuracao['opt_logo']['url']; ?>" alt="Logo Prohawk">
							<figcaption class="hidden">Logo Prohawk</figcaption>
						</figure>
					</a>
				</div>
				<!-- MENU -->	
				<div class="col-sm-9">
					<nav class="navbar">
						<ul>
							<li class="scrollTop">
								<a href="#secao-destaque">Home</a>
							</li>
							<li class="scrollTop">
								<a href="#secao-sobre">Sobre</a>
							</li>
							<li class="scrollTop">
								<a href="#secao-areas-atuacao">Área de atuação</a>
							</li>
							<li class="scrollTop">
								<a href="#secao-vantagens">Benefícios e Vantagens</a>
							</li>
							<li class="scrollTop">
								<a href="#secao-servicos">Serviços</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</header>