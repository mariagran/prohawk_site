<?php

	function base_modulos () {

		//TIPOS DE CONTEÚDO
		conteudos_prohawk();

		//TAXONOMIA
		// taxonomia_prohawk();

		//META BOXES
		metaboxes_prohawk();

	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/
	
		function conteudos_prohawk (){
		
			// CUSTOM POST TYPE ÁREA DE ATUAÇÃO
			tipoAreasAtuacao();

			// CUSTOM POST TYPE VANTAGENS
			tipoVantagens();

			// CUSTOM POST TYPE SERVIÇO
			tipoServicos();

		}

	/****************************************************
	* CUSTOM POST TYPE
	*****************************************************/

		// CUSTOM POST TYPE DESTAQUES
		function tipoAreasAtuacao() {

			$rotulosAreasAtuacao = array(
									'name'               => 'Áreas de atuação',
									'singular_name'      => 'areas_atuacao',
									'menu_name'          => 'Áreas de atuação',
									'name_admin_bar'     => 'Áreas de atuação',
									'add_new'            => 'Adicionar nova',
									'add_new_item'       => 'Adicionar nova área de atuação',
									'new_item'           => 'Nova área de atuação',
									'edit_item'          => 'Editar área de atuação',
									'view_item'          => 'Ver área de atuação',
									'all_items'          => 'Todas as áreas de atuação',
									'search_items'       => 'Buscar área de atuação',
									'parent_item_colon'  => 'Das áreas de atuação',
									'not_found'          => 'Nenhum destaque cadastrado.',
									'not_found_in_trash' => 'Nenhum destaque na lixeira.'
								);

			$argsAreasAtuacao 	= array(

									'labels'             => $rotulosAreasAtuacao,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-clipboard',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'areas_atuacao' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail','editor')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('areas_atuacao', $argsAreasAtuacao);

		}

		// CUSTOM POST TYPE PROJETOS
		function tipoVantagens() {

			$rotulosVantagens = array(
									'name'               => 'Vantagens',
									'singular_name'      => 'vantagem',
									'menu_name'          => 'Vantagens',
									'name_admin_bar'     => 'Vantagens',
									'add_new'            => 'Adicionar vantagem',
									'add_new_item'       => 'Adicionar novo vantagem',
									'new_item'           => 'Nova vantagem',
									'edit_item'          => 'Editar vantagem',
									'view_item'          => 'Ver vantagem',
									'all_items'          => 'Todas as vantagem',
									'search_items'       => 'Buscar vantagem',
									'parent_item_colon'  => 'Das vantagens',
									'not_found'          => 'Nenhuma vantagem cadastrado.',
									'not_found_in_trash' => 'Nenhuma vantagem na lixeira.'
								);

			$argsVantagens 	= array(
									'labels'             => $rotulosVantagens,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-yes-alt',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'vantagens' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('vantagem', $argsVantagens);

		}

		// CUSTOM POST TYPE SERVIÇO
		function tipoServicos() {

			$rotulosServicos = array(
									'name'               => 'Serviços',
									'singular_name'      => 'servico',
									'menu_name'          => 'Serviços',
									'name_admin_bar'     => 'Serviços',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo serviço',
									'new_item'           => 'Novo serviço',
									'edit_item'          => 'Editar serviço',
									'view_item'          => 'Ver serviço',
									'all_items'          => 'Todos os serviços',
									'search_items'       => 'Buscar serviços',
									'parent_item_colon'  => 'Dos serviços',
									'not_found'          => 'Nenhum serviço cadastrado.',
									'not_found_in_trash' => 'Nenhum serviço na lixeira.'
								);

			$argsServicos 	= array(
									'labels'             => $rotulosServicos,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-admin-tools',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'servicos' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('servico', $argsServicos);

		}

	/****************************************************
	* META BOXES
	*****************************************************/

		function metaboxes_prohawk(){
			add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );
		}

			function registraMetaboxes( $metaboxes ){

				// PREFIX
				$prefix = 'prohawk_';

				// METABOX  DESTAQUES
				$metaboxes[] = array(
					'id'			=> 'metaboxAreasAtuacao',
					'title'			=> 'Detalhes área de atuação',
					'pages' 		=> array('areas_atuacao'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
						array(
							'name'  => 'Imagem hover: ',
							'id'    => "{$prefix}icone_hover",
							'type'  => 'single_image'
						), 

						
					),

				);

				// METABOX  PROJETO
				// $metaboxes[] = array(
				// 	'id'			=> 'metaboxPlantas',
				// 	'title'			=> 'Detalhes planta',
				// 	'pages' 		=> array('planta'),
				// 	'context' 		=> 'normal',
				// 	'priority' 		=> 'high',
				// 	'autosave' 		=> false,
				// 	'fields' 		=> array(

				// 		array(
				// 			'name'   => 'Metragem da planta: ',
				// 			'id'     => "{$prefix}metragem_planta",
				// 			'type'   => 'text'
				// 		),

				// 		array(
				// 		    'id'      => "{$prefix}diferenciais_planta",
				// 		    'name'    => 'Diferenciais',
				// 		    'type'    => 'fieldset_text',

				// 		    // Options: array of key => Label for text boxes
				// 		    // Note: key is used as key of array of values stored in the database
				// 		    'options' => array(
				// 		        'icone'			=> 'Url - Ícone diferencial',
				// 		        'diferencial'	=> 'Título diferencial',
				// 		    ),
				// 		    // Is field cloneable?
				// 		    'clone'	=> true,
				// 		),

				// 		array(
				// 		    'id'      => "{$prefix}detalhes_sobre_planta",
				// 		    'name'    => 'Detalhes',
				// 		    'type'    => 'fieldset_text',

				// 		    // Options: array of key => Label for text boxes
				// 		    // Note: key is used as key of array of values stored in the database
				// 		    'options' => array(
				// 		        'icone'			=> 'Url - Ícone detalhe',
				// 		        'detalhe'	=> 'Título detalhe',
				// 		    ),
				// 		    // Is field cloneable?
				// 		    'clone'	=> true,
				// 		),

				// 		array(
				// 			'name'   => 'Imagem planta: ',
				// 			'id'     => "{$prefix}imagem_planta",
				// 			'type'   => 'single_image',
				// 		),

				// 		array(
				// 			'name'   => 'Imagem 3D planta: ',
				// 			'id'     => "{$prefix}imagem_3d_planta",
				// 			'type'   => 'single_image',
				// 		),

				// 	),

				// );

				// METABOX  SERVIÇO
				$metaboxes[] = array(
					'id'			=> 'metaboxServicos',
					'title'			=> 'Detalhes do serviço',
					'pages' 		=> array('servico'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(

						array(
							'name'   => 'Imagens serviço: ',
							'id'     => "{$prefix}galeria_servico",
							'type'   => 'image_advanced',
							'max_file_uploads' => 5,
						), 
						
					),

				);

				return $metaboxes;

			}

	/****************************************************
	* TAXONOMIA
	*****************************************************/
		function taxonomia_prohawk () {

			taxonomiaServicos();

		}

		function taxonomiaServicos() {

			$rotulosTaxonomiaServicos = array(
												'name'              => 'Categorias de serviço',
												'singular_name'     => 'Categorias de serviços',
												'search_items'      => 'Buscar categoria do serviço',
												'all_items'         => 'Todas as categorias',
												'parent_item'       => 'Categoria pai',
												'parent_item_colon' => 'Categoria pai:',
												'edit_item'         => 'Editar categoria do serviço',
												'update_item'       => 'Atualizar categoria',
												'add_new_item'      => 'Nova categoria',
												'new_item_name'     => 'Nova categoria',
												'menu_name'         => 'Categorias serviços',
											);

			$argsTaxonomiaServivos 		= array(

												'hierarchical'      => true,
												'labels'            => $rotulosTaxonomiaServicos,
												'show_ui'           => true,
												'show_admin_column' => true,
												'query_var'         => true,
												'rewrite'           => array( 'slug' => 'categoria-servicos' ),
											);

			register_taxonomy( 'categoriaservicos', array( 'servico' ), $argsTaxonomiaServivos);

		}

  	/****************************************************
	* AÇÕES
	*****************************************************/

		// INICIA A FUNÇÃO PRINCIPAL
		add_action('init', 'base_modulos');

		// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
		//add_action( 'add_meta_boxes', 'metaboxjs');

		// FLUSHS
		function rewrite_flush() {
	    	base_modulos();
	   		flush_rewrite_rules();
		}
		register_activation_hook( __FILE__, 'rewrite_flush' );