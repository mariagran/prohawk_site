<?php

/**
 * Template Name: Inicial
 * Description: Página Inicial
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Prohawk
 */

$areas_atuacao = new WP_Query( array( 'post_type' => 'areas_atuacao', 'oderby' => 'id', 'posts_per_page' => -1 ) );
$vantagens = new WP_Query( array( 'post_type' => 'vantagem', 'oderby' => 'id', 'posts_per_page' => -1 ) );
$servicos = new WP_Query( array( 'post_type' => 'servico', 'oderby' => 'id', 'posts_per_page' => -1 ) );

get_header(); ?>

<main class="pg pg-inicial">
	
	<section class="secao-destaque" id="secao-destaque">
		<h4 class="hidden">Seção destaque</h4>
		<video autoplay muted>
			<source src="<?php echo get_template_directory_uri(); ?>/img/valparaiso.mp4" type="video/mp4">
		</video>
		<div class="lente-video-destaque"></div>
		<div class="conteudo-destaque">
			<div class="large-container">
				<article>
					<div class="div-titulo">
						<h1 class="titulo"><?php echo $configuracao['opt_titulo_secao_destaque']; ?></h1>
					</div>
					<div class="div-descricao">
						<p><?php echo $configuracao['opt_descricao_secao_destaque']; ?></p>
						<a href="#">Saiba mais</a>
					</div>
					<div class="div-icones-video">
						<span class="icone-play-video"><img src="<?php echo get_template_directory_uri(); ?>/img/playb.svg" alt="Ícone vídeo"></span>
						<span class="icone-pause-video"><img src="<?php echo get_template_directory_uri(); ?>/img/pausew.svg" alt="Ícone vídeo"></span>
						<span class="icone-mute-video"><img src="<?php echo get_template_directory_uri(); ?>/img/sound.svg" alt="Ícone vídeo"></span>
					</div>
				</article>
			</div>
		</div>
	</section>

	<section class="secao-sobre" id="secao-sobre">
		<h4 class="hidden">Seção sobre</h4>
		<div class="titulo-sobre-prohawk">
			<div class="large-container">
				<div class="titulos-sobre">
					<h4 class="titulo titulo-secao">Sobre a empresa</h4>
					<h2 class="titulo"><?php echo $configuracao['opt_titulo_sobre']; ?></h2>
				</div>
			</div>
			<div class="full-container">
				<div class="conteudo-sobre">
					<figure class="desktop-element">
						<img src="<?php echo $configuracao['opt_imagem_sobre']['url']; ?>" alt="Imagem sobre">
						<figcaption class="hidden">Imagem sobre</figcaption>
					</figure>
					<div class="texto-sobre">
						<p><?php echo $configuracao['opt_texto_sobre']; ?></p>
						<ul class="selos-prohawk">

							<?php $selosId = $configuracao['opt_selos'];
							$selosId = explode(",", $selosId);
							foreach($selosId as $selo): $imageUrl = wp_get_attachment_image_src($selo)[0]; ?>
							<li><img src="<?php echo $imageUrl; ?>" alt="Selo Prohawk"></li>
							<?php endforeach; ?>

						</ul>
					</div>
					<figure class="mobile-element">
						<img src="<?php echo $configuracao['opt_imagem_sobre']['url']; ?>" alt="Imagem sobre">
						<figcaption class="hidden">Imagem sobre</figcaption>
					</figure>
				</div>
			</div>
		</div>
		<div class="full-container">
			<article class="sobre-selo-prohawk">
				<figure>
					<img src="<?php echo $configuracao['opt_icone_seguranca']['url']; ?>" alt="Selo Prohawk">
					<figcaption class="hidden">Selo prohawk</figcaption>
				</figure>
				<h3 class="titulo"><?php echo $configuracao['opt_titulo_seguranca']; ?></h3>
				<p><?php echo $configuracao['opt_descricao_seguranca']; ?></p>
				<div class="texto-importante">
					<p class="titulo red-text"><?php echo $configuracao['opt_titulo_importante_seguranca']; ?></p>
					<p class="red-text"><?php echo $configuracao['opt_p1_importante_seguranca']; ?></p>
					<p><?php echo $configuracao['opt_p2_importante_seguranca']; ?></p>
				</div>
			</article>
		</div>
	</section>

	<section class="secao-areas-atuacao" id="secao-areas-atuacao">
		<h4 class="hidden">Seção áreas de atuação</h4>
		<div class="areas-atuacao-background">
			<figure>
				<img src="<?php echo get_template_directory_uri(); ?>/img/" alt="Background áres de atuação" class="hidden">
				<figcaption class="hidden">Background áres de atuação</figcaption>
			</figure>
		</div>
		<div class="large-container">
			<article>
				<h4 class="titulo titulo-secao">Áreas de atuação</h4>
				<h2 class="titulo"><?php echo $configuracao['opt_titulo_areas_atuacao']; ?></h2>
				<ul class="areas-atuacao">
					
					<?php while($areas_atuacao->have_posts()): $areas_atuacao->the_post(); ?>
					<li>
						<img src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full')[0]; ?>" alt="<?php echo get_the_title(); ?>">
						<h3 class="titulo"><?php echo get_the_title(); ?></h3>
						<p><?php echo get_the_content(); ?></p>
						<span class="span-arrow"><img src="<?php echo get_template_directory_uri(); ?>/img/arrowb.svg" alt="Ícone seta"></span>
						<div class="container-formulario"></div>
					</li>
					<?php endwhile; wp_reset_query(); ?>

				</ul>
			</article>
		</div>
	</section>

	<section class="secao-vantagens" id="secao-vantagens">
		<h4 class="hidden">Seção benefícios e vantagens</h4>
		<div class="large-container">
			<article>
				<div class="titulo-vantagens">
					<h2 class="titulo"><?php echo $configuracao['opt_titulo_vantagens']; ?></h2>
				</div>
				<div class="itens-vantagens">
					<ul>

						<?php while($vantagens->have_posts()): $vantagens->the_post(); ?>
						<li>
							<img src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full')[0]; ?>" alt="<?php echo get_the_title(); ?>">
							<p><?php echo get_the_title(); ?></p>
						</li>
						<?php endwhile; wp_reset_query(); ?>

					</ul>
				</div>
			</article>
		</div>
	</section>

	<section class="secao-servicos" id="secao-servicos">
		<h4 class="hidden">Seção serviços</h4>
		<div class="produtos-gerados">
			<h4 class="titulo titulo-secao">Serviços</h4>
			<h2 class="titulo"><?php echo $configuracao['opt_titulo_servicos']; ?></h2>
			<ul class="desktop-element">

				<?php $contadorServicos = 0; while($servicos->have_posts()): $servicos->the_post(); ?>
				<li data-id="<?php echo $contadorServicos; ?>"><?php echo get_the_title(); ?></li>
				<?php $contadorServicos++; endwhile; wp_reset_query(); ?>

			</ul>
			<div class="menu-produtos-gerados mobile-element">
				<span class="span-produto-ativo">Nuvem de pontos altamente densificada;</span>
				<ul>
					
					<?php $contadorServicos = 0; while($servicos->have_posts()): $servicos->the_post(); ?>
					<li data-id="<?php echo $contadorServicos; ?>"><?php echo get_the_title(); ?></li>
					<?php $contadorServicos++; endwhile; wp_reset_query(); ?>

				</ul>
			</div>
		</div>
		
		<?php $contadorServicos = 0; while($servicos->have_posts()): $servicos->the_post(); $galeria = rwmb_meta('prohawk_galeria_servico'); ?>
		<div class="conteudo-produtos-gerados" id="<?php echo $contadorServicos; ?>">
			<div class="carrossel-servicos owl-carousel">
				
				<?php foreach($galeria as $imagem): ?>
				<figure>
					<img src="<?php echo $imagem['full_url']; ?>" alt="<?php echo get_the_title() ?>">
					<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
				</figure>
				<?php endforeach; ?>
				
			</div>
			<article>
				<div class="div-titulo-produtos">
					<h3 class="titulo"><?php echo get_the_title(); ?></h3>
				</div>
				<div class="div-texto-produtos">
					<p><?php echo get_the_content(); ?></p>
				</div>
				<div class="button-saiba-mais">
					<a href="#">Saiba mais</a>
				</div>
			</article>
		</div>
		<?php $contadorServicos++; endwhile; wp_reset_query(); ?>

	</section>

	<div class="div-pop-up-formulario">
		<div class="container-pop-up">
			<div class="conteudo-pop-up">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icone.png" alt="Ícone saneamento">
				<h3 class="titulo">Saneamento</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
				<span class="span-arrow"><img src="<?php echo get_template_directory_uri(); ?>/img/arrowb.svg" alt="Ícone seta"></span>
			</div>
			<div class="container-formulario"></div>
		</div>
	</div>

	<div class="formulario" id="formularioFaleConosco">
		<span class="fechar-formulario"><img src="<?php echo get_template_directory_uri(); ?>/img/close.svg" alt="Ícone fechar formulario"></span>
		<h4 class="titulo">Fale Conosco</h4>
		<?php echo do_shortcode('[contact-form-7 id="56" title="Fale conosco"]'); ?>
		<!-- <form>
			<label for="input-nome">
				<input type="text" placeholder="Nome" id="input-nome">
			</label>
			<label for="input-email">
				<input type="email" placeholder="Email" id="input-email">
			</label>
			<label for="input-celular">
				<input type="tel" placeholder="Celular" id="input-celular">
			</label>
			<div class="div-button-enviar">
				<input type="submit" value="Enviar">
			</div>
		</form> -->
	</div>

</main>

<?php get_footer();