<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Prohawk
 */

global $configuracao;

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

	<!-- META -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico" type="image/ico">

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<header>
		<div class="large-container">
			<div class="row">
				<!-- LOGO -->
				<div class="col-sm-3">
					<a class="logo" href="#">
						<figure>
							<img src="<?php echo $configuracao['opt_logo']['url']; ?>" alt="Logo Prohawk">
							<figcaption class="hidden">Logo Prohawk</figcaption>
						</figure>
					</a>
				</div>
				<!-- MENU -->	
				<div class="col-sm-9">
					<span class="open-menu-mobile">
						<img src="<?php echo get_template_directory_uri(); ?>/img/menu.svg" alt="Abrir menu">
					</span>
					<nav class="navbar">
						<span class="close-menu-mobile">
							<img src="<?php echo get_template_directory_uri(); ?>/img/x.svg" alt="Fechar menu">
						</span>
						<a class="logo-mobile" href="#">
							<figure>
								<img src="<?php echo $configuracao['opt_logo']['url']; ?>" alt="Logo Prohawk">
								<figcaption class="hidden">Logo Prohawk</figcaption>
							</figure>
						</a>
						<?php wp_nav_menu(); ?>
						<nav class="redes-sociais">
							<a href="<?php echo $configuracao['opt_facebook']; ?>" target="_blank">Facebook</a>
							<a href="<?php echo $configuracao['opt_instagram']; ?>" target="_blank">Instagram</a>
							<a href="<?php echo $configuracao['opt_youtube']; ?>" target="_blank">Youtube</a>
							<a href="<?php echo $configuracao['opt_linkedin']; ?>" target="_blank">LinkedIn</a>
						</nav>
					</nav>
				</div>
			</div>
		</div>
	</header>